import {AuthRequired} from '@/components'
import {
  Home,
  SignIn,
  SignUp,
  Profile,
  Notifications,
  Messages,
  Search,
  Edit,
  Tag,
  Logout,
} from '@/views'
import {BrowserRouter, Route, Switch} from 'react-router-dom'

// Main app router
export default function Router() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/signin">
          <SignIn />
        </Route>
        <Route exact path="/signup">
          <SignUp />
        </Route>
        <Route exact path="/">
          <AuthRequired to="/signin">
            <Home />
          </AuthRequired>
        </Route>
        <Route exact path="/profile/:id">
          <AuthRequired to="/signin">
            <Profile />
          </AuthRequired>
        </Route>
        <Route exact path="/tag/:id">
          <AuthRequired to="/signin">
            <Tag />
          </AuthRequired>
        </Route>
        <Route exact path="/notifications">
          <AuthRequired to="/signin">
            <Notifications />
          </AuthRequired>
        </Route>
        <Route exact path="/messages">
          <AuthRequired to="/signin">
            <Messages />
          </AuthRequired>
        </Route>
        <Route exact path="/search">
          <AuthRequired to="/signin">
            <Search />
          </AuthRequired>
        </Route>
        <Route exact path="/edit">
          <AuthRequired to="/signin">
            <Edit />
          </AuthRequired>
        </Route>
        <Route exact path="/logout">
          <AuthRequired to="/signin">
            <Logout />
          </AuthRequired>
        </Route>
      </Switch>
    </BrowserRouter>
  )
}
