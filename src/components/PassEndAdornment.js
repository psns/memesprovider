import {InputAdornment, IconButton} from '@material-ui/core'
import {Visibility, VisibilityOff} from '@material-ui/icons'

// PassEndAdornment define the eye logo at the end of the password field.
export default function PassEndAdornment({onClick, showPass}) {
  return (
    <InputAdornment position="end">
      <IconButton
        onClick={() => onClick()}
        onMouseDown={e => e.preventDefault()}
      >
        {showPass ? <Visibility /> : <VisibilityOff />}
      </IconButton>
    </InputAdornment>
  )
}
