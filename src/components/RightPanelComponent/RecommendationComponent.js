import {Grid, makeStyles, Paper} from '@material-ui/core'

const useStyle = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: '0',
  },
  title: {
    color: '#352c44',
    width: '300px',
    marginTop: '0',
    marginLeft: '0',
  },
  gridElement: {
    border: '1px solid #352c44',
    backgroundColor: '#EEEEEE',
    elevation: '3',
    width: '100%',
    height: '35px',
  },
  text: {
    paddingTop: '8px',
    paddingLeft: '4px',
    height: '100%',
  },
}))
const Recommendations = [
  {
    text: 'blabla1',
  },
  {
    text: 'blabla2',
  },
  {
    text: 'blabla3',
  },
  {
    text: 'blabla4',
  },
  {
    text: 'blabla5',
  },
]
export default function RecommendationComponent() {
  const classes = useStyle()
  return (
    <div className={classes.root}>
      <div className={classes.title}>
        <h2>Recommendations</h2>
      </div>
      <Grid
        container
        spacing={1}
        direction="column"
        justify="center"
        alignItems="left"
      >
        {Recommendations.map((recommendationItem, index) => (
          <Grid item xs={12} key={index}>
            <Paper className={classes.gridElement}>
              <div className={classes.text}>{recommendationItem.text}</div>
            </Paper>
          </Grid>
        ))}
      </Grid>
    </div>
  )
}
