import {TextField,makeStyles} from '@material-ui/core'
import InputAdornment from '@material-ui/core/InputAdornment'
import {Search} from '@material-ui/icons'


const useStyle = makeStyles(theme => ({
    root: {
      width: '100%',
    },
}))

export default function SearchField() {
    const classes = useStyle()
  return (
    <TextField
      id="input-with-icon-textfield"
      label="Search"
      className={classes.root}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <Search />
          </InputAdornment>
        ),
      }}
    />
  )
}
