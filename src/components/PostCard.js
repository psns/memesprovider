/* eslint-disable react-hooks/exhaustive-deps */
import {
  Avatar,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  IconButton,
  makeStyles,
  Typography,
  Menu,
  MenuItem,
} from '@material-ui/core'
import {Api} from '@/components'
import {
  HeartOutline,
  Heart,
  LockOutline,
  AccountGroupOutline,
  DeleteOutline,
  ImageText,
} from 'mdi-material-ui'
import clsx from 'clsx'
import {useState, useEffect, useContext} from 'react'
import {AppContext, MagnifyComment} from '@/utils'
import parse from 'html-react-parser'
import {useHistory, useLocation} from 'react-router-dom'

const useStyle = makeStyles(themes => ({
  root: {
    width: '99,9%',
    backgroundColor: '#EEEEEE',
    borderBottom: '1px solid #352c44',
    borderRight: '1px solid #352c44',
    borderRadius: '0',
  },
  avatar: {
    border: `2px solid ${themes.palette.primary[themes.palette.type]}`,
  },
  media: {
    height: 0,
    paddingTop: '56.25%',
  },
  actionText: {
    color: 'grey',
  },
  likeIcon: {
    color: themes.palette.primary[themes.palette.type],
  },
}))

export default function PostCard({postId = 0}) {
  const classes = useStyle()
  const history = useHistory()
  const location = useLocation()
  const {user} = useContext(AppContext)
  const [post, setPost] = useState(null)
  const [anchorEl, setAnchorEl] = useState(null)

  const liked = post?.likes?.length > 0 ?? false

  function loadPostInfo() {
    return Api.v1.posts
      .getUnique(postId, {requestor: user.id})
      .then(newPost => setPost(newPost))
  }

  function handleMetaClick(e) {
    setAnchorEl(e.currentTarget)
  }

  function handleMetaClose() {
    setAnchorEl(null)
  }

  function handleDeleteClick() {
    Api.v1.posts.delete(postId).then(() => {
      history.push('/random')
      history.push(location)
    })
  }

  function handleLikeClick(e) {
    e.stopPropagation()
    if (liked) {
      Api.v1.unLikePost(postId).then(() => loadPostInfo())
    } else {
      Api.v1.likePost(postId).then(() => loadPostInfo())
    }
  }

  useEffect(() => {
    loadPostInfo()
  }, [postId])

  return post != null ? (
    <Card className={clsx(classes.root, 'btn-effect')}>
      <CardHeader
        avatar={
          <Avatar
            className={classes.avatar}
            src={
              post.author.avatar
                ? `/api/v1/uploads/v1/perm/avatar/${
                    post.authorId
                  }?random=${new Date().getTime()}` // This avoid image cache by browser
                : ''
            }
          />
        }
        action={[
          <IconButton disabled key={0}>
            {post.private ? <LockOutline /> : <AccountGroupOutline />}
          </IconButton>,
          post.medias.length > 0 ? (
            <IconButton onClick={handleMetaClick} key={1}>
              <ImageText />
            </IconButton>
          ) : (
            <span key={1}></span>
          ),
          post.authorId === user.id ? (
            <IconButton key={2} onClick={handleDeleteClick}>
              <DeleteOutline />
            </IconButton>
          ) : (
            <span key={2}></span>
          ),
        ]}
        title={post.author.username}
        subheader={'@' + post.author.tag}
      />
      {(() =>
        post.medias.length > 0 ? (
          <CardMedia
            className={classes.media}
            image={`/api/v1/uploads/v1/perm/${
              post.medias[0].id
            }?random=${new Date().getTime()}`}
          />
        ) : (
          <></>
        ))()}

      <CardContent>
        <Typography variant="body2" color="textSecondary" id="cardContent">
          {parse(MagnifyComment(post.comment))}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="favButton" onClick={handleLikeClick}>
          {liked ? (
            <Heart className={classes.likeIcon} />
          ) : (
            <HeartOutline className={classes.likeIcon} />
          )}
        </IconButton>
        <span className={classes.actionText}>{post._count.likes}</span>
        <span className={classes.actionText} style={{marginLeft: '20px'}}>
          {post._count.replies} Comment
          {post._count.replies.length > 0 ? 's' : ''}
        </span>
      </CardActions>
      {post.medias.length > 0 ? (
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleMetaClose}
        >
          {Object.keys(post.medias[0]).map((key, id) => (
            <MenuItem key={id}>
              {key}: {post.medias[0][key]}
            </MenuItem>
          ))}
        </Menu>
      ) : (
        <></>
      )}
    </Card>
  ) : (
    <div></div>
  )
}
