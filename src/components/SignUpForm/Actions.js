import {makeStyles, Button} from '@material-ui/core'
import {Link} from 'react-router-dom'

const useStyles = makeStyles(theme => ({
  actionContainer: {
    width: '325px',
    margin: 'auto',
    display: 'flex',
  },
  flexRegister: {
    width: '150px',
  },
  flexSignIn: {
    textAlign: 'left',
    width: '100%',
    paddingTop: '8px',
    paddingLeft: '9px',
    fontFamily: 'Roboto',
  },
  registerLink: {
    textDecoration: 'none',
    color: theme.palette.primary[theme.palette.type],
  },
}))

export default function Action({onSignUp = () => {}, disabled = true}) {
  const classes = useStyles()

  return (
    <div className={classes.actionContainer}>
      <div className={classes.flexSignIn}>
        <Link to="/signin" className={classes.registerLink}>
          Sign in
        </Link>
      </div>
      <div className={classes.flexRegister}>
        <Button variant="contained" color="primary" disabled={disabled} onClick={onSignUp}>
          Sign Up
        </Button>
      </div>
    </div>
  )
}
