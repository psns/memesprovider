import {useState, useContext} from 'react'
import {makeStyles} from '@material-ui/core'
import Credentials from './Credentials'
import Actions from './Actions'
import {Api} from '@/components'
import {useHistory} from 'react-router-dom'
import {AppContext} from '@/utils'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'block',
  },
  titleContainer: {
    width: '325px',
    margin: 'auto',
    textAlign: 'center',
    marginBottom: '20px',
  },
}))

export default function SignUpForm() {
  const classes = useStyles()
  const {setLogged, setUser} = useContext(AppContext)
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [tagName, setTagName] = useState('')
  const [userName, setUserName] = useState('')
  const [email, setEmail] = useState('')
  const [emailConfirmation, setEmailConfirmation] = useState('')
  const [password, setPassword] = useState('')
  const [passwordConfirmation, setPasswordConfirmation] = useState('')
  const [validForm, setValidForm] = useState(false)

  const [tagRules, setTagRules] = useState([
    val => !/[^a-zA-Z0-9]/.test(val) || 'Only alphanumeric',
  ])
  const [emailRules, setEmailRules] = useState([
    val =>
      /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/.test(val) || 'Email address format',
  ])

  const history = useHistory()

  function handleSignUp() {
    Api.v1
      .register({
        email,
        password,
        username: userName,
        tag: tagName,
        firstName,
        lastName,
      })
      .then(res => setUser(res))
      .then(() => setLogged(true))
      .then(() => history.push('/'))
      .catch(e => {
        if (e.response.data.code === 'P2002') {
          if (e.response.data.meta.target[0] === 'email')
            setEmailRules([
              ...emailRules,
              val => val !== email || 'Email already exist',
            ])
          if (e.response.data.meta.target[0] === 'tag')
            setTagRules([
              ...tagRules,
              val => val !== tagName || 'Tag already exist',
            ])
        }
      })
  }

  return (
    <div className={classes.root}>
      <div className={classes.titleContainer}>
        <img
          src={process.env.PUBLIC_URL + '/img/MP_title.png'}
          alt="Memes provider title logo"
          width="275px"
        />
      </div>

      <div>
        <form noValidate autoComplete="off">
          <Credentials
            firstName={firstName}
            lastName={lastName}
            userName={userName}
            tagRules={tagRules}
            tagName={tagName}
            email={email}
            emailConfirmation={emailConfirmation}
            emailRules={emailRules}
            password={password}
            passwordConfirmation={passwordConfirmation}
            onFirstNameChange={setFirstName}
            onLastNameChange={setLastName}
            onUserNameChange={setUserName}
            onTagNameChange={setTagName}
            onEmailChange={setEmail}
            onEmailConfirmationChange={setEmailConfirmation}
            onPasswordChange={setPassword}
            onPasswordConfirmationChange={setPasswordConfirmation}
            onValidChange={valid => {
              if (valid !== validForm) setValidForm(valid)
            }}
          />
          <div style={{height: '15px'}}></div>
          <Actions disabled={!validForm} onSignUp={handleSignUp}></Actions>
        </form>
      </div>
    </div>
  )
}
