import React, {useEffect, useState} from 'react'
import {makeStyles, FormControl, Grid, InputAdornment} from '@material-ui/core'
import {MTextField} from '@/components'
import PassEndAdornment from '../PassEndAdornment'

const useStyles = makeStyles(theme => ({
  textField: {
    margin: theme.spacing(1),
    width: '325px',
  },
  littleTextField: {
    margin: theme.spacing(1),
    width: '155px',
  },
  grid: {
    alignItems: 'center',
  },
}))

export default function Credentials({
  onFirstNameChange,
  firstName,
  onLastNameChange,
  lastName,
  onUserNameChange,
  userName,
  onEmailChange,
  email,
  onEmailConfirmationChange,
  emailConfirmation,
  onPasswordChange,
  password,
  onPasswordConfirmationChange,
  passwordConfirmation,
  onTagNameChange,
  tagName,
  onValidChange,
  emailRules = [],
  tagRules = []
}) {
  const [showPass, setShowPass] = useState(false)
  const classes = useStyles()
  let validForm = true

  useEffect(() => {
    if (onValidChange != null) onValidChange(validForm)
  })

  return (
    <React.Fragment>
      <Grid container>
        <Grid item xs={12} className={classes.grid}>
          <FormControl>
            <MTextField
              required
              className={classes.littleTextField}
              label="FirstName"
              value={firstName}
              onChange={e => onFirstNameChange(e.target.value)}
              onValidChange={valid => (validForm = validForm && valid)}
              variant="filled"
              size="small"
            />
          </FormControl>
          <FormControl>
            <MTextField
              required
              className={classes.littleTextField}
              label="LastName"
              value={lastName}
              onChange={e => onLastNameChange(e.target.value)}
              onValidChange={valid => (validForm = validForm && valid)}
              variant="filled"
              size="small"
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl>
            <MTextField
              required
              rules={[val => val.length < 11 || 'Your userName is too long']}
              className={classes.littleTextField}
              label="UserName"
              value={userName}
              onChange={e => onUserNameChange(e.target.value)}
              onValidChange={valid => (validForm = validForm && valid)}
              variant="filled"
              size="small"
              helperText="Ex : Atares"
            />
          </FormControl>
          <FormControl>
            <MTextField
              required
              rules={tagRules}
              className={classes.littleTextField}
              label="TagName"
              value={tagName}
              onChange={e => onTagNameChange(e.target.value)}
              onValidChange={valid => (validForm = validForm && valid)}
              variant="filled"
              size="small"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">@</InputAdornment>
                ),
              }}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl>
            <MTextField
              required
              rules={emailRules}
              className={classes.textField}
              label="Email"
              value={email}
              onChange={e => onEmailChange(e.target.value)}
              onValidChange={valid => (validForm = validForm && valid)}
              variant="filled"
              size="small"
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl>
            <MTextField
              required
              rules={[val => val === email || "Emails doesn't match"]}
              className={classes.textField}
              label="Email confimation"
              value={emailConfirmation}
              onChange={e => onEmailConfirmationChange(e.target.value)}
              onValidChange={valid => (validForm = validForm && valid)}
              variant="filled"
              size="small"
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl>
            <MTextField
              required
              rules={[val => val.length >= 10 || 'Your password is too short']}
              className={classes.textField}
              label="Password"
              value={password}
              onChange={e => onPasswordChange(e.target.value)}
              onValidChange={valid => (validForm = validForm && valid)}
              variant="filled"
              size="small"
              type={showPass ? 'text' : 'password'}
              InputProps={{
                endAdornment: (
                  <PassEndAdornment
                    onClick={() => setShowPass(!showPass)}
                    showPass={showPass}
                  />
                ),
              }}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl>
            <MTextField
              required
              rules={[val => val === password || 'Different password']}
              className={classes.textField}
              label="Password confimation"
              value={passwordConfirmation}
              onChange={e => onPasswordConfirmationChange(e.target.value)}
              onValidChange={valid => (validForm = validForm && valid)}
              variant="filled"
              size="small"
              type={showPass ? 'text' : 'password'}
              InputProps={{
                endAdornment: (
                  <PassEndAdornment
                    onClick={() => setShowPass(!showPass)}
                    showPass={showPass}
                  />
                ),
              }}
            />
          </FormControl>
        </Grid>
      </Grid>
    </React.Fragment>
  )
}
