import {
  ListItem,
  ListItemAvatar,
  ListItemText,
  Avatar,
  Card,
} from '@material-ui/core'

export default function SearchResult({
  avatarUrl = '',
  title = '',
  subtitle = '',
  hasAvatar = true,
  onFollow,
  onClick,
}) {
  return (
    <Card style={{borderRadius: '0', backgroundColor: '#eee'}}>
      <ListItem
        className="search-result"
        onClick={e => {
          if (onClick != null) onClick(e)
        }}
      >
        {hasAvatar ? (
          <ListItemAvatar>
            <Avatar src={avatarUrl}></Avatar>
          </ListItemAvatar>
        ) : (
          <></>
        )}

        <ListItemText primary={title} secondary={subtitle} />
      </ListItem>
    </Card>
  )
}
