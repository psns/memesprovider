import {useContext, useState} from 'react'
import {makeStyles} from '@material-ui/core'
import Credentials from './Credentials'
import Actions from './Actions'
import {useHistory} from 'react-router-dom'
import {AppContext} from '@/utils'
import Api from '../Api'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'block',
  },
  titleContainer: {
    width: '325px',
    margin: 'auto',
    textAlign: 'center',
    marginBottom: '20px',
  },
  errorMessage: {
    fontFamily: 'Roboto',
    color: '#FF665B',
  },
}))

export default function LoginForm() {
  const {setLogged, setUser} = useContext(AppContext)
  const classes = useStyles()
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [loginErr, setLoginErr] = useState(false)
  const history = useHistory()

  function handleSignIn() {
    Api.v1
      .login(email, password)
      .then(user => setUser(user))
      .then(() => setLoginErr(false))
      .then(() => setLogged(true))
      .then(() => history.push('/'))
      .catch(() => setLoginErr(true))
  }

  return (
    <div className={classes.root}>
      <div className={classes.titleContainer}>
        <img
          src={process.env.PUBLIC_URL + '/img/MP_title.png'}
          alt="Memes provider title logo"
          width="275px"
        />
      </div>

      <div>
        <form noValidate autoComplete="off">
          <Credentials
            login={email}
            password={password}
            onLoginChange={setEmail}
            onPasswordChange={setPassword}
          />
          {loginErr ? (
            <span className={classes.errorMessage}>
              Login/password incorrect
            </span>
          ) : (
            <></>
          )}
          <div style={{height: '15px'}}></div>
          <Actions onSignIn={() => handleSignIn()} />
        </form>
      </div>
    </div>
  )
}
