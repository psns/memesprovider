import React from 'react'
import {makeStyles, FormControl, TextField, Grid} from '@material-ui/core'
import PassEndAdornment from '../PassEndAdornment'

const useStyles = makeStyles(theme => ({
  textField: {
    margin: theme.spacing(1),
    width: '325px',
  },
}))

export default function Credentials({
  onLoginChange,
  login,
  onPasswordChange,
  password,
}) {
  const classes = useStyles()

  const [showPass, setShowPass] = React.useState(false)

  return (
    <React.Fragment>
      <Grid container>
        <Grid item xs={12}>
          <FormControl>
            <TextField
              className={classes.textField}
              label="Login"
              value={login}
              onChange={e => onLoginChange(e.target.value)}
              variant="filled"
              size="small"
            />
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl>
            <TextField
              className={classes.textField}
              type={showPass ? 'text' : 'password'}
              label="Password"
              value={password}
              onChange={e => onPasswordChange(e.target.value)}
              variant="filled"
              size="small"
              InputProps={{
                endAdornment: (
                  <PassEndAdornment
                    onClick={() => setShowPass(!showPass)}
                    showPass={showPass}
                  />
                ),
              }}
            />
          </FormControl>
        </Grid>
      </Grid>
    </React.Fragment>
  )
}
