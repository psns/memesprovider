import {makeStyles, Button} from '@material-ui/core'
import {Link} from 'react-router-dom'

const useStyles = makeStyles(theme => ({
  actionContainer: {
    width: '325px',
    margin: 'auto',
    display: 'flex',
  },
  flexSignIn: {
    width: '150px',
  },
  flexRegister: {
    textAlign: 'right',
    width: '100%',
    paddingTop: '8px',
    paddingRight: '9px',
    fontFamily: 'Roboto',
  },
  registerLink: {
    textDecoration: 'none',
    color: theme.palette.primary[theme.palette.type],
  },
}))

export default function Action({onSignIn}) {
  const classes = useStyles()

  return (
    <div className={classes.actionContainer}>
      <div className={classes.flexSignIn}>
        <Button variant="contained" color="primary" onClick={() => onSignIn()}>
          Sign In
        </Button>
      </div>

      <div className={classes.flexRegister}>
        Not registered ? &nbsp;
        <Link to="/signup" className={classes.registerLink}>
          Sign up
        </Link>
      </div>
    </div>
  )
}
