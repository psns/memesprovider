import {Fragment, useState, useContext} from 'react'
import {Grid, Button, makeStyles} from '@material-ui/core'
import {Alert, AlertTitle} from '@material-ui/lab'
import {Api} from '@/components'
import {AppContext} from '@/utils'

const useStyles = makeStyles(theme => ({
  right: {
    textAlign: 'right',
  },
  left: {
    textAlign: 'left',
  },
  action: {
    width: '100%',
    textAlign: 'right',
  },
}))

export default function Abi() {
  const classes = useStyles()
  const {user} = useContext(AppContext)
  const [alertSuccess, setAlertSuccess] = useState(false)
  const [alertWarn, setAlertWarn] = useState(false)
  const [alertError, setAlertError] = useState(false)

  function handleSave(e) {
    const formData = new FormData()
    const avatarFile = document.querySelector('#avatar')
    const backgroundFile = document.querySelector('#background')

    // Exit if no file were upload
    if (avatarFile.files.length === 0 && backgroundFile.files.length === 0) {
      setAlertWarn(true)
      setTimeout(() => setAlertWarn(false), 2000)
      return
    }

    if (avatarFile.files.length !== 0)
      formData.append('avatar', avatarFile.files[0])
    if (backgroundFile.files.length !== 0)
      formData.append('background', backgroundFile.files[0])

    Api.v1
      .postMedia(formData)
      .then(() => Api.v1.profiles.set(user.id))
      .then(() => {
        setAlertSuccess(true)
        setTimeout(() => setAlertSuccess(false), 2000)
      })
      .catch(e => {
        setAlertError(true)
        setTimeout(() => setAlertError(false), 2000)
      })

    document.querySelector('#avatar').value = null
    document.querySelector('#background').value = null
  }

  return (
    <Fragment>
      <h2>Avatar and background image</h2>
      <hr></hr>
      <Grid container>
        <Grid item xs={6} className={classes.left}>
          <label htmlFor="avatar">Change avatar:</label>
        </Grid>
        <Grid item xs={6} className={classes.right}>
          <input
            type="file"
            id="avatar"
            name="avatar"
            accept="image/png, image/jpeg"
          ></input>
        </Grid>
      </Grid>
      <br></br>
      <Grid container>
        <Grid item xs={6} className={classes.left}>
          <label htmlFor="background">Change Background:</label>
        </Grid>
        <Grid item xs={6} className={classes.right}>
          <input
            type="file"
            id="background"
            name="background"
            accept="image/png, image/jpeg"
          ></input>
        </Grid>
      </Grid>
      <br></br>
      <br></br>
      <div className={classes.action}>
        <Button
          variant="contained"
          style={{color: '#eee', backgroundColor: '#8AA248'}}
          onClick={handleSave}
        >
          Save
        </Button>
      </div>
      <br></br>
      {(() => {
        if (alertError)
          return (
            <Alert severity="error">
              <AlertTitle>Error occurred, please try again</AlertTitle>
            </Alert>
          )
        if (alertSuccess)
          return (
            <Alert severity="success">
              <AlertTitle>Changes applied</AlertTitle>
            </Alert>
          )
        if (alertWarn)
          return (
            <Alert severity="warning">
              <AlertTitle>Please choose file first</AlertTitle>
            </Alert>
          )
        return <></>
      })()}
    </Fragment>
  )
}
