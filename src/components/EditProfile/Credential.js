/* eslint-disable react-hooks/exhaustive-deps */
import {Fragment, useState, useContext, useEffect} from 'react'
import {Grid, Button, makeStyles} from '@material-ui/core'
import {Alert, AlertTitle} from '@material-ui/lab'
import {Api, MTextField} from '@/components'
import {AppContext} from '@/utils'

const useStyles = makeStyles(theme => ({
  right: {
    textAlign: 'right',
  },
  left: {
    textAlign: 'left',
  },
  action: {
    width: '100%',
    textAlign: 'right',
  },
}))

export default function Credential() {
  const classes = useStyles()
  const {user, setUser} = useContext(AppContext)
  const [alertSuccess, setAlertSuccess] = useState(false)
  const [alertError, setAlertError] = useState(false)
  const [valid, setValid] = useState(true)

  const [emailRules, setEmailRules] = useState([
    val =>
      /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/.test(val) || 'Email address format',
  ])
  const [email, setEmail] = useState(user.credential.email)
  const [confirmEmail, setConfirmEmail] = useState(user.credential.email)
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  let validForm = true

  function handleSave(e) {
    const req = {email}
    if (password !== '') req.password = password

    Api.v1.profiles
      .set(user.id, req)
      .then(newUser => {
        setUser(newUser)
        setAlertSuccess(true)
        setTimeout(() => setAlertSuccess(false), 2000)
      })
      .catch(e => {
        if (e.response.data.code === 'P2002')
          setEmailRules([
            ...emailRules,
            val => val !== email || 'Email already exist',
          ])
        setAlertError(true)
        setTimeout(() => setAlertError(false), 2000)
      })
  }

  useEffect(() => {
    if (valid !== validForm) setValid(validForm)
  })

  return (
    <Fragment>
      <h2>Credentials</h2>
      <hr></hr>
      <Grid container spacing={5}>
        <Grid item xs={6}>
          <MTextField
            style={{width: '100%'}}
            required
            rules={emailRules}
            label="Email"
            value={email}
            onChange={e => setEmail(e.target.value)}
            onValidChange={valid => (validForm = validForm && valid)}
          />
        </Grid>
        <Grid item xs={6}>
          <MTextField
            style={{width: '100%'}}
            required
            rules={[val => val === email || "Emails doesn't match"]}
            label="Email confirmation"
            value={confirmEmail}
            onChange={e => setConfirmEmail(e.target.value)}
            onValidChange={valid => (validForm = validForm && valid)}
          />
        </Grid>
      </Grid>
      <br></br>
      <Grid container spacing={5}>
        <Grid item xs={6}>
          <MTextField
            id="dulol"
            style={{width: '100%'}}
            required
            rules={[val => val.length > 10 || 'Your password is too short']}
            label="Password"
            value={password}
            onChange={e => setPassword(e.target.value)}
            onValidChange={valid => (validForm = validForm && valid)}
            type="password"
            helperText="You can change your password by setting another in both fields"
          />
        </Grid>
        <Grid item xs={6}>
          <MTextField
            style={{width: '100%'}}
            required
            rules={[val => val === password || 'Different password']}
            label="Password confirmation"
            value={confirmPassword}
            onChange={e => setConfirmPassword(e.target.value)}
            onValidChange={valid => (validForm = validForm && valid)}
            type="password"
          />
        </Grid>
      </Grid>
      <br></br>
      <br></br>
      <div className={classes.action}>
        <Button
          variant="contained"
          style={{
            color: '#eee',
            backgroundColor: valid ? '#8AA248' : 'grey',
          }}
          onClick={handleSave}
          disabled={!valid}
        >
          Save
        </Button>
      </div>
      <br></br>
      {(() => {
        if (alertError)
          return (
            <Alert severity="error">
              <AlertTitle>Error occurred, please try again</AlertTitle>
            </Alert>
          )
        if (alertSuccess)
          return (
            <Alert severity="success">
              <AlertTitle>Changes applied</AlertTitle>
            </Alert>
          )
        return <></>
      })()}
    </Fragment>
  )
}
