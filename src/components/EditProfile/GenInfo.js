/* eslint-disable react-hooks/exhaustive-deps */
import {Fragment, useState, useContext, useEffect} from 'react'
import {Grid, Button, InputAdornment, makeStyles} from '@material-ui/core'
import {Alert, AlertTitle} from '@material-ui/lab'
import {Api, MTextField} from '@/components'
import {AppContext} from '@/utils'

const useStyles = makeStyles(theme => ({
  right: {
    textAlign: 'right',
  },
  left: {
    textAlign: 'left',
  },
  action: {
    width: '100%',
    textAlign: 'right',
  },
}))

export default function GenInfo() {
  const classes = useStyles()
  const {user, setUser} = useContext(AppContext)
  const [alertSuccess, setAlertSuccess] = useState(false)
  const [alertError, setAlertError] = useState(false)
  const [valid, setValid] = useState(true)

  const [tagRules, setTagRules] = useState([
    val => !/[^a-zA-Z0-9]/.test(val) || 'Only alphanumeric',
  ])
  const [firstName, setFirstName] = useState(user.firstName)
  const [lastName, setLastName] = useState(user.lastName)
  const [tag, setTagName] = useState(user.tag)
  const [username, setUsername] = useState(user.username)
  const [description, setDescription] = useState(user.description)

  let validForm = true

  function handleSave(e) {
    Api.v1.profiles
      .set(user.id, {firstName, lastName, tag, username, description})
      .then(newUser => {
        setUser(newUser)
        setAlertSuccess(true)
        setTimeout(() => setAlertSuccess(false), 2000)
      })
      .catch(e => {
        if (e.response.data.code === 'P2002')
          setTagRules([...tagRules, val => val !== tag || 'Tag already exist'])
        setAlertError(true)
        setTimeout(() => setAlertError(false), 2000)
      })
  }

  useEffect(() => {
    if (valid !== validForm) setValid(validForm)
  })

  return (
    <Fragment>
      <h2>General information</h2>
      <hr></hr>
      <Grid container spacing={5}>
        <Grid item xs={6}>
          <MTextField
            style={{width: '100%'}}
            required
            label="FirstName"
            value={firstName}
            onChange={e => setFirstName(e.target.value)}
            onValidChange={valid => (validForm = validForm && valid)}
          />
        </Grid>
        <Grid item xs={6}>
          <MTextField
            style={{width: '100%'}}
            required
            label="LastName"
            value={lastName}
            onChange={e => setLastName(e.target.value)}
            onValidChange={valid => (validForm = validForm && valid)}
          />
        </Grid>
      </Grid>
      <br></br>
      <Grid container spacing={5}>
        <Grid item xs={6}>
          <MTextField
            style={{width: '100%'}}
            required
            label="Username"
            value={username}
            onChange={e => setUsername(e.target.value)}
            onValidChange={valid => (validForm = validForm && valid)}
          />
        </Grid>
        <Grid item xs={6}>
          <MTextField
            style={{width: '100%'}}
            required
            rules={tagRules}
            label="Tag"
            value={tag}
            onChange={e => setTagName(e.target.value)}
            onValidChange={valid => (validForm = validForm && valid)}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">@</InputAdornment>
              ),
            }}
            helperText="Must be unique, other users can use it to search your profile."
          />
        </Grid>
      </Grid>
      <br></br>
      <Grid container>
        <Grid item xs={12}>
          <MTextField
            style={{width: '100%'}}
            label="Description"
            value={description}
            onChange={e => setDescription(e.target.value)}
            onValidChange={valid => (validForm = validForm && valid)}
            multiline
          />
        </Grid>
      </Grid>
      <br></br>
      <br></br>
      <div className={classes.action}>
        <Button
          variant="contained"
          style={{
            color: '#eee',
            backgroundColor: valid ? '#8AA248' : 'grey',
          }}
          onClick={handleSave}
          disabled={!valid}
        >
          Save
        </Button>
      </div>
      <br></br>
      {(() => {
        if (alertError)
          return (
            <Alert severity="error">
              <AlertTitle>Error occurred, please try again</AlertTitle>
            </Alert>
          )
        if (alertSuccess)
          return (
            <Alert severity="success">
              <AlertTitle>Changes applied</AlertTitle>
            </Alert>
          )
        return <></>
      })()}
    </Fragment>
  )
}
