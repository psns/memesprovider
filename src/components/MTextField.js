import {TextField} from '@material-ui/core'
import {useState} from 'react'

export default function MTextField({
  rules = [],
  value,
  helperText = '',
  errorText = '',
  required,
  onChange,
  onValidChange,
  ...rest
}) {
  const [first, setFirst] = useState(true)

  let errorMessage = ''

  if (required != null && required)
    rules.push(val => val.length > 0 || 'This field is required')
  let valid = rules.every(
    rule => typeof (errorMessage = rule(value)) === 'boolean',
  )

  if (onValidChange != null) onValidChange(valid)

  return (
    <TextField
      {...rest}
      value={value}
      required={required}
      error={!valid && !first}
      helperText={valid || first ? helperText : errorMessage}
      onChange={e => {
        setFirst(false)
        if (onChange != null) onChange(e)
      }}
    />
  )
}
