import {AppContext} from '@/utils'
import React from 'react'
import {Grid, Button, Menu, MenuItem, Fade} from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu'

export default function BottomLeftComponent() {
  const [anchorEl, setAnchorEl] = React.useState(null)
  const open = Boolean(anchorEl)

  const handleClick = event => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }
  return (
    <div>
      <Grid
        container
        spacing={6}
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Grid item xs>
          <img
            src={process.env.PUBLIC_URL + '/img/MP_noback_purple_noshadow.png'}
            alt="Memes provider logo"
            width="100px"
          />
        </Grid>
        <Grid item xs>
          <Button
            aria-controls="fade-menu"
            aria-haspopup="true"
            onClick={handleClick}
          >
            <MenuIcon></MenuIcon>
          </Button>
          <Menu
            id="fade-menu"
            anchorEl={anchorEl}
            keepMounted
            open={open}
            onClose={handleClose}
            TransitionComponent={Fade}
          >
            <AppContext.Consumer>
              {({setLogged}) => (
                <MenuItem onClick={() => setLogged(false)}>Log out</MenuItem>
              )}
            </AppContext.Consumer>
          </Menu>
        </Grid>
      </Grid>
    </div>
  )
}
