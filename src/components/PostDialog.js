import {
  Dialog,
  DialogContent,
  DialogTitle,
  DialogActions,
  Button,
  Grid,
  RadioGroup,
  FormControlLabel,
  Radio,
  makeStyles,
} from '@material-ui/core'
import {MTextField, Api} from '@/components'
import {useState} from 'react'
import {FindTagsInStr} from '@/utils'

const useStyles = makeStyles(theme => ({
  root: {
    width: '300px',
  },
  title: {
    color: theme.palette.primary[theme.palette.type],
  },
}))

export default function PostDialog({
  onClose = () => {},
  replyTo = -1,
  ...rest
}) {
  const classes = useStyles()
  const [postFile, setPostFile] = useState(null)
  const [comment, setComment] = useState('')
  const [privatePost, setPrivatePost] = useState('public')

  const postDisabled = postFile === null && comment === ''

  function handleCommentChange({target: {value}}) {
    setComment(value)
  }

  function handleMediaChange(e) {
    const media = document.querySelector('#media')
    if (media.files.length !== 0) setPostFile(media.files[0])
    else setPostFile(null)
  }

  function handleClose() {
    Api.v1.clearSessionMedias()
    setComment('')
    setPostFile(null)
    onClose()
  }

  async function handlePost() {
    if (postFile !== null) {
      const formData = new FormData()
      formData.append('media', postFile)
      await Api.v1.postMedia(formData)
    }

    const tags = FindTagsInStr(comment)
    Api.v1
      .createPost(comment, tags, privatePost === 'private', replyTo !== -1 ? replyTo : undefined)
      .then(() => handleClose())
  }

  return (
    <Dialog {...rest} onClose={handleClose}>
      <DialogTitle className={classes.title}>
        Create a post
        <RadioGroup
          row
          value={privatePost}
          onChange={e => setPrivatePost(e.target.value)}
          color="primary"
        >
          <FormControlLabel
            value="private"
            control={<Radio color="primary" />}
            label="Private"
          />
          <FormControlLabel
            value="public"
            control={<Radio color="primary" />}
            label="Public"
          />
        </RadioGroup>
        <span style={{color: 'grey', fontSize: '0.7em'}}>
          {privatePost === 'private'
            ? 'Only the people you follow can see'
            : 'Everyone can see'}
        </span>
      </DialogTitle>
      <DialogContent style={{width: '500px', fontFamily: 'Roboto'}}>
        <Grid container>
          <Grid item xs={6} className={classes.left}>
            <label htmlFor="avatar">Add an image:</label>
          </Grid>
          <Grid item xs={6} className={classes.right}>
            <input
              type="file"
              id="media"
              name="media"
              accept="image/png, image/jpeg"
              onChange={handleMediaChange}
            ></input>
          </Grid>
        </Grid>
        <br></br>
        <Grid container>
          <Grid item xs={12}>
            <MTextField
              style={{width: '100%'}}
              label="Comment"
              value={comment}
              onChange={handleCommentChange}
              helperText="Tip: You can put tags on your post by prefixing a word with #"
              multiline
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button style={{color: '#FF665B'}} onClick={handleClose}>
          Cancel
        </Button>
        <Button
          style={{
            color: '#eee',
            backgroundColor: !postDisabled ? '#8AA248' : '#ddd',
          }}
          autoFocus
          disabled={postDisabled}
          onClick={handlePost}
        >
          Post
        </Button>
      </DialogActions>
    </Dialog>
  )
}
