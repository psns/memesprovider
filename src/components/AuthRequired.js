/* eslint-disable react-hooks/exhaustive-deps */
import {AppContext} from '@/utils'
import {useState, useEffect, useContext} from 'react'
import {Redirect} from 'react-router-dom'
import {Api} from '@/components'

export default function AuthRequired({children, to}) {
  const [child, setChild] = useState(<></>)
  const {logged, setLogged, setUser} = useContext(AppContext)

  useEffect(() => {
    Api.v1
      .logged()
      .then(user => setUser(user))
      .then(() => setLogged(true))
      .then(() => setChild(children))
      .catch(() => {
        console.clear()
        setChild(<Redirect to={to} />)
      })
  }, [logged])

  return logged ? children : child
}
