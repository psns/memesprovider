import {makeStyles} from '@material-ui/core'
import {PostCard, Api} from '@/components'
import {useState, useEffect, useContext} from 'react'
import {AppContext} from '@/utils'
import {useParams} from 'react-router-dom'

const useStyle = makeStyles(theme => ({
  memeLine: {
    maxHeight: '47vh',
    width: '100%',
  },
}))

export default function ProfileLikes() {
  const classes = useStyle()
  const [displayedPosts, setDisplayedPosts] = useState([])
  const [page, setPage] = useState(1)
  const [count, setCount] = useState(5)
  const {id} = useParams()

  useEffect(() => {
    Api.v1.profiles
      .getProfilePosts(id, {count, page: page - 1})
      .then(posts => setDisplayedPosts(posts))
  }, [id])

  return (
    <div className={classes.memeLine}>
      {displayedPosts.map((post, index) => (
        <PostCard post={post} key={index} />
      ))}
    </div>
  )
}
