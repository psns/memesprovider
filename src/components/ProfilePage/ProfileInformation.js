/* eslint-disable react-hooks/exhaustive-deps */
import {useState, Fragment, useContext, useEffect} from 'react'
import {Grid, makeStyles, Avatar, Container} from '@material-ui/core'
import {MiddleComponentTitle, Api} from '@/components'
import {AppContext} from '@/utils'
import {useParams, useHistory} from 'react-router-dom'

const useStyles = makeStyles(theme => ({
  grid: {
    height: '225px',
    width: '100%',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
  },
  btnContainer: {
    textAlign: 'right',
  },
  secondGrid: {
    marginTop: theme.spacing(7),
  },
  avatar: {
    width: theme.spacing(12),
    height: theme.spacing(12),
    marginLeft: theme.spacing(2),
    marginTop: theme.spacing(22),
    border: `3px solid ${theme.palette.primary[theme.palette.type]}`,
    backgroundColor: '#eee',
  },
  follow: {
    fontWeight: 'bold',
    fontSize: '15px',
  },
}))

export default function ProfileInformation({
  userInfo,
  onActionClicked = () => {},
}) {
  const history = useHistory()
  const classes = useStyles()
  const {user} = useContext(AppContext)
  const {id} = useParams()
  const [isFollowing, setIsFollowing] = useState(false)

  function handleAction(e) {
    e.stopPropagation()
    if (user.id === Number(id)) {
      history.push('/edit')
    } else if (isFollowing) {
      Api.v1
        .unFollow(userInfo.id)
        .then(() => setIsFollowing(false))
        .then(() => onActionClicked())
    } else {
      Api.v1
        .follow(userInfo.id)
        .then(() => setIsFollowing(true))
        .then(() => onActionClicked())
    }
  }

  useEffect(() => {
    if (user.id !== Number(id)) {
      Api.v1.profiles
        .getProfileFollowed(user.id, {inId: [userInfo.id]})
        .then(profiles => {
          if (profiles.length !== 0) {
            setIsFollowing(true)
          }
        })
    } else {
      setIsFollowing(false)
    }
  }, [userInfo])

  return (
    <Fragment>
      <MiddleComponentTitle backBtn>{userInfo.username}</MiddleComponentTitle>
      <Grid
        container
        className={classes.grid}
        style={
          userInfo.background
            ? {
                backgroundImage: `url(/api/v1/uploads/v1/perm/background/${
                  userInfo.id
                }?random=${new Date().getTime()})`,
              }
            : {
                backgroundColor: 'grey',
              }
        }
      >
        <Grid item xs={6}>
          <Avatar
            className={classes.avatar}
            src={
              userInfo.avatar
                ? `/api/v1/uploads/v1/perm/avatar/${
                    userInfo.id
                  }?random=${new Date().getTime()}` // This avoid image cache by browser
                : ''
            }
          />
        </Grid>
      </Grid>
      <Container>
        <Grid container className={classes.secondGrid}>
          <Grid item xs={6}>
            <span style={{fontWeight: 'bold', fontSize: '20px'}}>
              {userInfo.username}
            </span>
            <br></br>@{userInfo.tag}
          </Grid>
          <Grid item xs={6} className={classes.btnContainer}>
            <button
              type="button"
              className={
                isFollowing ? 'following-btn-light' : 'follow-btn-light'
              }
              onClick={handleAction}
            >
              {(() => {
                if (user.id === Number(id)) return 'Edit profile'
                else if (isFollowing) return 'Following'
                return 'Follow'
              })()}
            </button>
          </Grid>
        </Grid>
        <div>{userInfo.description}</div>
        <br></br>
        <Grid container direction="row">
          <Grid item xs={2} className={classes.follow}>
            {userInfo._count.following} following
          </Grid>
          <Grid item xs={10} className={classes.follow}>
            {userInfo._count.followers} followers
          </Grid>
        </Grid>
      </Container>
    </Fragment>
  )
}
