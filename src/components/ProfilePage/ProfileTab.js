import {Fragment} from 'react'
import {PostCard} from '@/components'
import {Tab, Tabs, makeStyles} from '@material-ui/core'
import TabPanel from './TabPanel'

const useStyles = makeStyles(theme => ({
  root: {
    borderBottom: '1px solid #CFCFCF',
    position: 'sticky',
    top: '60px',
    backgroundColor: '#eee',
    zIndex: '99',
  },
  memeLine: {
    maxHeight: '47vh',
    width: '100%',
  },
}))
function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  }
}

export default function ProfileTab({
  posts = [],
  likes = [],
  value = 0,
  onChange = () => {},
}) {
  const classes = useStyles()
  const handleChange = (event, newValue) => {
    onChange(newValue)
  }
  return (
    <Fragment>
      <Tabs
        value={value}
        onChange={handleChange}
        aria-label="tab"
        centered
        indicatorColor="primary"
        className={classes.root}
      >
        <Tab label="Memes" {...a11yProps(0)} />
        <Tab label="Likes" {...a11yProps(1)} />
      </Tabs>
      <TabPanel value={value} index={0}>
        <div className={classes.memeLine}>
          {posts.map((post, index) => (
            <PostCard key={index} postId={post.id} />
          ))}
        </div>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <div className={classes.memeLine}>
          {likes.map((post, index) => (
            <PostCard key={index} postId={post.id} />
          ))}
        </div>
      </TabPanel>
    </Fragment>
  )
}
