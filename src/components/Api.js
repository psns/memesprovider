import axios from 'axios'

/* eslint-disable no-unused-vars */

class StdListQuery {
  count
  page
}

class RequestorQuery {
  requestor
}

class ExtendedListQuery extends StdListQuery {
  requestor
}

// Query options
class ProfileListQuery {
  tag
  username
}

// Query options
class ProfileFollowedQuery extends StdListQuery {
  tag
  username
  inId = []
}

class PostListQuery extends ExtendedListQuery {
  comment
}

class TagListQuery extends StdListQuery {
  name
}

// Basic profile
class Profile {
  id
  tag
  avatar
  background
  username
  lastName
  description
  createdAt
}

// Profile with count
class ProfileWithCount extends Profile {
  _count = {
    followers: 0,
    following: 0,
    followedTags: 0,
    posts: 0,
  }
}

class ProfileOnTagResult {
  profileId
  tagId
  createdAt
  profile = new Profile()
}

class FollowerResult {
  followerId
  followedId
  followedAt
  follower = new Profile()
}
class FollowedResult {
  followerId
  followedId
  followedAt
  followed = new Profile()
}

class Tag {
  id
  name
  createdAt
}

class TagWithCount extends Tag {
  _count = {
    posts: 0,
    followers: 0,
  }
}

class TagOnPostResult {
  postId
  tagId
  createdAt
  tag = new Tag()
}

class Media {
  id
  postId
  tempUrl
  size
  encoding
  mimetype
  md5
  createdAt
}

class Post {
  id
  authorId
  author = new Profile()
  comment
  parentId
  createdAt
  /**
   * @type {Array<Media>}
   */
  medias
  /**
   * @type {Array<TagOnPostResult>}
   */
  tags
  /**
   * @type {Array<Object{createdAt}>}
   */
  likes
}

class PostWithCount extends Post {
  _count = {
    replies: 0,
    likes: 0,
  }
}

class LikesOnPostResult {
  profileId
  postId
  createdAt
  profile = new Profile()
}

/* eslint-enable no-unused-vars */

const api = {
  v1: {
    /**
     * SignIn the user and return user's info.
     * @param {String} email
     * @param {String} password
     * @returns {Promise<ProfileWithCount>}
     */
    login: (email, password) =>
      axios.post('/api/v1/signin', {email, password}).then(res => res.data),

    /**
     * Logout the user
     */
    logout: () => axios.get('/api/v1/logout'),

    /**
     * Register the user
     * @param {Object} info See back validator for details
     * @returns
     */
    register: info => axios.post('/api/v1/signup', info).then(res => res.data),

    /**
     * Test if browser is logged.
     */
    logged: () => axios.get('/api/v1/logged').then(res => res.data),

    /**
     * Follow a user.
     * @param {Number} profileId
     * @returns {Promise}
     */
    follow: profileId => axios.post(`/api/v1/follows/${profileId}`),

    /**
     * Follow a user.
     * @param {Number} profileId
     * @returns {Promise}
     */
    unFollow: profileId => axios.delete(`/api/v1/follows/${profileId}`),

    /**
     * Create a tag.
     * @param {String} name
     * @returns {Promise}
     */
    createTag: name => axios.post('/api/v1/tags', {name}),

    /**
     * Follow a tag.
     * @param {Number} tagId
     * @returns {Promise}
     */
    followTag: tagId => axios.post(`/api/v1/tags/${tagId}/follow`),

    /**
     * UnFollow a tag.
     * @param {Number} tagId
     * @returns {Promise}
     */
    unFollowTag: tagId => axios.delete(`/api/v1/tags/${tagId}/follow`),

    /**
     * Create a posts.
     * @param {Number} tagId
     * @param {Array<String>} tags,
     * @param {Number} parentId
     * @returns {Promise}
     */
    createPost: (comment, tags = [], privatePost = false, parentId) =>
      axios.post('/api/v1/posts', {
        comment,
        tags,
        privatePost,
        parentId,
      }),

    /**
     * Like a post.
     * @param {Number} postId
     * @returns {Promise}
     */
    likePost: postId => axios.post(`/api/v1/posts/${postId}/like`),

    /**
     * UnLike a post.
     * @param {Number} postId
     * @returns {Promise}
     */
    unLikePost: postId => axios.delete(`/api/v1/posts/${postId}/like`),

    postMedia: formData =>
      axios.post('/api/v1/medias', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      }),

    /**
     * Clear tmp file stored in session's object,
     * must be called when the user cancel a post creation.
     * @param {Number} postId
     * @returns {Promise}
     */
    clearSessionMedias: () => axios.delete('/api/v1/medias'),

    profiles: {
      /**
       *  List profiles, can be filtered with options.
       * @param {ProfileListQuery} options
       * @returns {Promise<Array<ProfileWithCount>>}
       */
      list: options =>
        axios.get('/api/v1/profiles', {params: options}).then(res => res.data),

      /**
       * Update profile infos
       * @param {Number} profileId
       * @param {Profile} options
       * @returns {Promise<Array<Profile>>}
       */
      set: (profileId, options) =>
        axios
          .put(`/api/v1/profiles/${profileId}`, options)
          .then(res => res.data),

      /**
       * Returns details about a single profile.
       * @param {Number} profileId
       * @returns {Promise<ProfileWithCount>}
       */
      getUnique: profileId =>
        axios.get(`/api/v1/profiles/${profileId}`).then(res => res.data),

      /**
       * Return meme line of a profile.
       * @param {Number} profileId
       * @param {ExtendedListQuery} options
       */
      getLine: (profileId, options) =>
        axios
          .get(`/api/v1/profiles/${profileId}/line`, {params: options})
          .then(res => res.data),

      /**
       * List followers of a profile, can be filtered with options.
       * @param {Number} profileId
       * @param {ProfileListQuery} options
       * @returns {Promise<Array<FollowerResult>>}
       */
      getProfileFollowers: (profileId, options) =>
        axios
          .get(`/api/v1/profiles/${profileId}/followers`, {params: options})
          .then(res => res.data),

      /**
       * List followed of a profile, can be filtered with options.
       * @param {Number} profileId
       * @param {ProfileFollowedQuery} options
       * @returns {Promise<Array<FollowedResult>>}
       */
      getProfileFollowed: (profileId, options) =>
        axios
          .get(`/api/v1/profiles/${profileId}/followed`, {
            params: {
              ...options,
              inId: JSON.stringify(options.inId),
            },
          })
          .then(res => res.data),

      /**
       * List posts of a profile, can be filtered with options.
       * @param {Number} profileId
       * @param {PostListQuery} options
       * @returns {Promise<Array<PostWithCount>>}
       */
      getProfilePosts: (profileId, options) =>
        axios
          .get(`/api/v1/profiles/${profileId}/posts`, {params: options})
          .then(res => res.data),

      /**
       * List likes of a profile, can be filtered with options.
       * @param {Number} profileId
       * @param {PostListQuery} options
       * @returns {Promise<Array<PostWithCount>>}
       */
      getProfileLikes: (profileId, options) =>
        axios
          .get(`/api/v1/profiles/${profileId}/likes`, {params: options})
          .then(res => res.data),
    },
    tags: {
      /**
       * List tags, can be filtered with options.
       * @param {TagListQuery} options
       * @returns {Promise<Array<TagWithCount>>}
       */
      list: options =>
        axios.get('/api/v1/tags', {params: options}).then(res => res.data),

      /**
       * Returns details about a single tag.
       * @param {Number} tagId
       * @returns {Promise<TagWithCount>}
       */
      getUnique: tagId =>
        axios.get(`/api/v1/tags/${tagId}`).then(res => res.data),

      /**
       * List followers of a tag, can be filtered with options.
       * @param {Number} tagId
       * @param {ProfileListQuery} options
       * @returns {Promise<Array<ProfileOnTagResult>>}
       */
      getTagFollowers: (tagId, options) =>
        axios
          .get(`/api/v1/tags/${tagId}/followers`, {params: options})
          .then(res => res.data),

      /**
       * List posts of a tag, can be filtered with options.
       * @param {Number} tagId
       * @param {PostListQuery} options
       * @returns {Promise<Array<PostWithCount>>}
       */
      getTagPosts: (tagId, options) =>
        axios
          .get(`/api/v1/tags/${tagId}/posts`, {params: options})
          .then(res => res.data),
    },
    posts: {
      /**
       * List posts, can be filtered with options.
       * @param {Number} profileId
       * @param {PostListQuery} options
       * @returns {Promise<Array<PostWithCount>>}
       */
      list: options =>
        axios.get('/api/v1/posts', {params: options}).then(res => res.data),

      /**
       *  Delete a post with the specified Id.
       * @param {Number} postId
       * @returns
       */
      delete: postId => axios.delete(`/api/v1/posts/${postId}`),

      /**
       * Returns details about a single post.
       * @param {Number} postId
       * @param {RequestorQuery} options
       * @returns {Promise<PostWithCount>}
       */
      getUnique: (postId, options) =>
        axios
          .get(`/api/v1/posts/${postId}`, {params: options})
          .then(res => res.data),

      /**
       * List likes of a post, can be filtered with options.
       * @param {Number} postId
       * @param {ProfileListQuery} options
       * @returns {Promise<Array<LikesOnPostResult>>}
       */
      getPostLikes: (postId, options) =>
        axios
          .get(`/api/v1/posts/${postId}/likes`, {params: options})
          .then(res => res.data),

      /**
       * List replies of a post, can be filtered with options.
       * @param {Number} postId
       * @param {StdListQuery} options
       * @returns {Promise<Array<PostWithCount>>}
       */
      getPostReplies: (postId, options) =>
        axios
          .get(`/api/v1/posts/${postId}/replies`, {params: options})
          .then(res => res.data),
    },
  },
}

export default api
