import {Container, makeStyles} from '@material-ui/core'
import {Link} from 'react-router-dom'
import {ArrowLeft} from 'mdi-material-ui'

const useStyle = makeStyles(theme => ({
  title: {
    border: '1px solid #352c44',
    padding: '15px 0 15px 15px',
    fontSize: '1.5em',
    width: '100%',
    color: '#eee',
    fontFamily: 'Comfortaa',
    backgroundColor: theme.palette.primary[theme.palette.type],
    height: '60px',
    maxHeight: '60px',
    position: 'sticky',
    top: '0px',
    zIndex: '99',
  },
  action: {
    marginTop: '10px',
    display: 'inline',
  },
  content: {display: 'inline'},
}))

export default function MiddleComponentTitle({
  children,
  backBtn = false,
  to = '/',
}) {
  const classes = useStyle()
  return (
    <Container className={classes.title}>
      {backBtn ? (
        <div className={classes.action}>
          <Link to={to}>
            <span style={{verticalAlign: 'middle', marginRight: '10px'}}>
              <ArrowLeft style={{color: '#eee'}} />
            </span>
          </Link>
        </div>
      ) : (
        <></>
      )}
      <div className={classes.content}>{children}</div>
    </Container>
  )
}
