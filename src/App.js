import {ThemeProvider, AppProvider} from '@/utils'
import Router from './Router'

/**
 * Entry point component
 */
export default function App() {
  return (
    <ThemeProvider>
      <AppProvider>
        <Router />
      </AppProvider>
    </ThemeProvider>
  )
}
