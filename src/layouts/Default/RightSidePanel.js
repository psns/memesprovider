import {makeStyles} from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
    width: '100%',
  },
  gridElement: {
    border: '1px solid #352c44',
    borderRadius: '30px',
    marginRight: 'auto',
    fontFamily: 'Comfortaa-Bold',
    width: '350px',
  },
  searchInput: {
    backgroundColor: '#EEEEEE',
    marginLeft: '0',
  },
}))

export default function RightSideComponent() {
  const classes = useStyles()
  return <div className={classes.root}></div>
}
