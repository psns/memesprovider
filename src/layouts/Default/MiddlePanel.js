import {makeStyles} from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
    width: '100%',
    color: '#352c44',
    fontFamily: 'Roboto',
  },
}))

export default function MiddleComponent({children}) {
  const classes = useStyles()
  return <div className={classes.root}>{children}</div>
}
