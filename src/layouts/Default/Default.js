import {makeStyles} from '@material-ui/core/styles'
import {Grid} from '@material-ui/core'
import LeftSidePanel from './LeftSidePanel'
import MiddleComponent from './MiddlePanel'
import RightSideComponent from './RightSidePanel'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: '#EEEEEE',
  },
  gridElement: {
    border: '1px solid #352c44',
  },
}))

export default function Default({children}) {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Grid
        container
        spacing={0}
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Grid item xs={4}>
          <LeftSidePanel />
        </Grid>
        <Grid item xs={4} className={classes.gridElement}>
          <MiddleComponent>{children}</MiddleComponent>
        </Grid>
        <Grid item xs={4}>
          <RightSideComponent></RightSideComponent>
        </Grid>
      </Grid>
    </div>
  )
}
