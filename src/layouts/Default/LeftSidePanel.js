import {makeStyles, Container} from '@material-ui/core'
import {NavLink, Link, useLocation, useHistory} from 'react-router-dom'
import {
  Home,
  EmailOutline,
  BellOutline,
  AccountOutline,
  Magnify,
  Feather,
  Logout,
} from 'mdi-material-ui'
import {AppContext} from '@/utils'
import {useContext, useState} from 'react'
import {PostDialog} from '@/components'

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    display: 'flex',
    alignItems: 'right',
    height: '100vh',
  },
  action: {
    marginLeft: 'auto',
    marginRight: '0',
  },
  actionContainer: {
    display: 'block',
  },
  navLinkIcon: {
    verticalAlign: 'middle',
    marginRight: '20px',
  },
  image: {
    width: '60px',
    marginTop: '20px',
    paddingLeft: '6px',
  },
}))

export default function LeftSidePanel() {
  const classes = useStyles()
  const location = useLocation()
  const history = useHistory()
  const {user} = useContext(AppContext)
  const [openPostDialog, setOpenPostDialog] = useState(false)

  const routes = [
    {
      title: 'Home',
      path: '/',
      icon: <Home style={{fontSize: 30}} />,
    },
    {
      title: 'Messages',
      path: '/messages',
      icon: <EmailOutline style={{fontSize: 30}} />,
    },
    {
      title: 'Notifications',
      path: '/notifications',
      icon: <BellOutline style={{fontSize: 30}} />,
    },
    {
      title: 'Search',
      path: '/search',
      icon: <Magnify style={{fontSize: 30}} />,
    },
    {
      title: 'Profile',
      path: `/profile/${user.id}`,
      icon: <AccountOutline style={{fontSize: 30}} />,
    },
  ]

  return (
    <div className={classes.root}>
      <div className={classes.action}>
        <Container>
          <img
            src={process.env.PUBLIC_URL + '/img/MP_noback_purple_noshadow.png'}
            className={classes.image}
            alt="Memes provider logo"
          />
          <div style={{height: '30px'}}></div>
          {routes.map((route, index) => (
            <div key={index}>
              <NavLink
                to={route.path}
                className={
                  'action-btn-light ' +
                  (location.pathname === route.path
                    ? 'current-a-btn-light'
                    : '')
                }
              >
                <span className={classes.navLinkIcon}>{route.icon}</span>
                {route.title}
              </NavLink>
            </div>
          ))}
          <button
            type="button"
            className="post-btn-light"
            onClick={() => setOpenPostDialog(true)}
          >
            <span style={{verticalAlign: 'middle', marginRight: '10px'}}>
              <Feather style={{fontSize: 30}} />
            </span>
            Post
          </button>
          <div style={{width: '100%', textAlign: 'center', paddingTop: '20px'}}>
            <Link to="/logout">
              <Logout />
              Logout
            </Link>
          </div>
        </Container>
      </div>
      <PostDialog
        open={openPostDialog}
        onClose={() => {
          setOpenPostDialog(false)
          history.push('/random')
          history.push(location)
        }}
      />
    </div>
  )
}
