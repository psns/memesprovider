import {DefaultLayout} from '@/layouts'
import {MiddleComponentTitle, Abi, GenInfo, EditCred} from '@/components'
import {Container, makeStyles} from '@material-ui/core'
import {AppContext} from '@/utils'
import {useContext} from 'react'

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: '25px',
    maxHeight: '93vh',
    overflowY: 'auto',
  },
  avatar: {
    width: theme.spacing(15),
    height: theme.spacing(15),
  },
}))

export default function Edit() {
  const classes = useStyles()
  const {user} = useContext(AppContext)
  return (
    <DefaultLayout>
      <MiddleComponentTitle backBtn to={`/profile/${user.id}`}>
        Edit profile
      </MiddleComponentTitle>
      <Container className={classes.root}>
        <Abi />
        <GenInfo />
        <EditCred />
      </Container>
    </DefaultLayout>
  )
}
