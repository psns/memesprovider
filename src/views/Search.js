import {useState} from 'react'
import {DefaultLayout} from '@/layouts'
import {MTextField, SearchResult, MiddleComponentTitle, Api} from '@/components'
import {
  InputAdornment,
  Container,
  Card,
  RadioGroup,
  FormControlLabel,
  Radio,
  List,
  CircularProgress,
  makeStyles,
} from '@material-ui/core'
import {Magnify} from 'mdi-material-ui'
import {useHistory} from 'react-router-dom'

const useStyles = makeStyles(theme => ({
  searchContainer: {
    display: 'block',
    width: '100%',
    backgroundColor: '#eee',
    borderBottom: '1px solid black',
  },
  searchBar: {
    marginTop: '20px',
    width: '100%',
    color: '#eee',
  },
  resultContainer: {
    display: 'block',
    width: '100%',
    overflowY: 'auto',
    overflowX: 'hidden',
    maxHeight: '81vh',
  },
  loaderContainer: {
    width: '100%',
    textAlign: 'center',
    paddingTop: '75px',
  },
}))

let typeTimer = null

export default function Search() {
  const classes = useStyles()
  const [search, setSearch] = useState('')
  const [filter, setFilter] = useState('profile')
  const [profileResult, setProfileResult] = useState([])
  const [tagResult, setTagResult] = useState([])
  const [loading, setLoading] = useState(false)
  const history = useHistory()

  function searchOnApi(value, type) {
    if (type === 'profile') {
      Api.v1.profiles
        .list({tag: value, username: value})
        .then(profiles => setProfileResult(profiles))
        .then(() => setLoading(false))
    } else if (type === 'tag') {
      Api.v1.tags
        .list({name: value})
        .then(tags => setTagResult(tags))
        .then(() => setLoading(false))
    }
  }

  function handleFilterChange(e) {
    setFilter(e.target.value)
    setProfileResult([])
    setTagResult([])
    if (search !== '') {
      searchOnApi(search, e.target.value)
      setLoading(true)
    }
  }

  function handleSearchChange(e) {
    if (typeTimer != null) clearTimeout(typeTimer)
    typeTimer = setTimeout(() => searchOnApi(e.target.value, filter), 250)
    setSearch(e.target.value)
    setLoading(true)
  }

  return (
    <DefaultLayout>
      <MiddleComponentTitle backBtn>Search</MiddleComponentTitle>
      <Card style={{borderRadius: '0'}} elevation={0}>
        <Container className={classes.searchContainer}>
          <MTextField
            value={search}
            onChange={handleSearchChange}
            label="Search..."
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Magnify></Magnify>
                </InputAdornment>
              ),
            }}
            className={classes.searchBar}
            variant="outlined"
          ></MTextField>
          <RadioGroup
            row
            value={filter}
            onChange={handleFilterChange}
            color="primary"
          >
            <FormControlLabel
              value="profile"
              control={<Radio color="primary" />}
              label="Profile"
              color="primary"
            />
            <FormControlLabel
              value="tag"
              control={<Radio color="primary" />}
              label="Tag"
            />
          </RadioGroup>
        </Container>
      </Card>

      <div className={classes.resultContainer}>
        {loading ? (
          <div className={classes.loaderContainer}>
            <CircularProgress color="primary" />
          </div>
        ) : (
          <List>
            {filter === 'profile'
              ? profileResult.map(profile => (
                  <SearchResult
                    key={profile.id}
                    avatarUrl={
                      profile.avatar
                        ? `/api/v1/uploads/v1/perm/avatar/${
                            profile.id
                          }?random=${new Date().getTime()}` // This avoid image cache by browser
                        : ''
                    }
                    title={profile.username}
                    subtitle={`@${profile.tag}`}
                    onClick={() => history.push(`/profile/${profile.id}`)}
                  ></SearchResult>
                ))
              : tagResult.map(tag => (
                  <SearchResult
                    key={tag.id}
                    title={`#${tag.name}`}
                    subtitle={`${tag._count.posts} posts`}
                    onClick={() => history.push(`/tag/${tag.id}`)}
                    hasAvatar={false}
                  ></SearchResult>
                ))}
          </List>
        )}
      </div>
    </DefaultLayout>
  )
}
