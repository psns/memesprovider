import {DefaultLayout} from '@/layouts'
import {MiddleComponentTitle} from '@/components'

export default function Notifications() {
  return (
    <DefaultLayout>
      <MiddleComponentTitle backBtn>Notifications</MiddleComponentTitle>
      <h1>Feature not available now</h1>
    </DefaultLayout>
  )
}
