import {Redirect} from 'react-router-dom'
import {Api} from '@/components'

export default function Logout() {
    Api.v1.logout()
  return <Redirect to="/signin" />
}
