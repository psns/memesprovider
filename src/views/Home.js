/* eslint-disable react-hooks/exhaustive-deps */
import {MiddleComponentTitle, PostCard, Api} from '@/components'
import {DefaultLayout} from '@/layouts'
import {makeStyles} from '@material-ui/core'
import {useState, useContext, useEffect} from 'react'
import {AppContext} from '@/utils'

const useStyle = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  title: {
    border: '1px solid #352c44',
    width: '100%',
    backgroundColor: '#EEEEEE',
  },
  memeLine: {
    overflowY: 'auto',
    overflowX: 'hidden',
    maxHeight: '93vh',
  },
}))

export default function Home() {
  const [displayedPosts, setDisplayedPosts] = useState([])
  const [page, setPage] = useState(1)
  const count = 25
  const {user} = useContext(AppContext)
  const classes = useStyle()

  function handleScroll(e) {
    const bottom =
      e.target.clientHeight === e.target.scrollHeight - e.target.scrollTop

    if (bottom && page < 7) {
      Api.v1.profiles.getLine(user.id, {count, page}).then(posts => {
        if (posts.length !== 0) {
          setDisplayedPosts(displayedPosts.concat(posts))
          setPage(page + 1)
        }
      })
    }
  }

  useEffect(() => {
    Api.v1.profiles
      .getLine(user.id, {count, page: page - 1, requestor: user.id})
      .then(posts => {
        setDisplayedPosts(posts)
      })
  }, [])

  return (
    <DefaultLayout>
      <div className={classes.root}>
        <MiddleComponentTitle>Home</MiddleComponentTitle>
        <div className={classes.memeLine} onScroll={handleScroll}>
          {displayedPosts.map((post, index) => (
            <PostCard postId={post.id} key={index} />
          ))}
        </div>
      </div>
    </DefaultLayout>
  )
}
