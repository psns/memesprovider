import home from './Home'
import signIn from './SignIn'
import signUp from './SignUp'
import profile from './Profile'
import notifications from './Notifications'
import messages from './Messages'
import search from './Search'
import edit from './Edit'
import tag from './Tag'
import logout from './Logout'

export const Home = home
export const SignIn = signIn
export const SignUp = signUp
export const Profile = profile
export const Notifications = notifications
export const Messages = messages
export const Search = search
export const Edit = edit
export const Tag = tag
export const Logout = logout
