import {LoginForm} from '@/components'
import {makeStyles, Grid} from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  leftPanel: {
    textAlign: 'center',
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.palette.primary[theme.palette.type],
  },
  rightPanel: {
    height: '100vh',
    backgroundColor: theme.palette.background[theme.palette.type],
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}))

export default function SignIn() {
  const classes = useStyles()
  return (
    <Grid container spacing={0}>
      <Grid item xs={6} className={classes.leftPanel}>
        <img
          src={process.env.PUBLIC_URL + '/img/MP_noback_noshadow.png'}
          alt="Memes provider logo"
          width="400px"
        />
      </Grid>
      <Grid item xs={6} className={classes.rightPanel}>
        <LoginForm />
      </Grid>
    </Grid>
  )
}
