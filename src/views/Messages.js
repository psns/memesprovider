import {DefaultLayout} from '@/layouts'
import {MiddleComponentTitle} from '@/components'

export default function Messages() {
  return (
    <DefaultLayout>
      <MiddleComponentTitle backBtn>Messages</MiddleComponentTitle>
      <h1>Feature not available now</h1>
    </DefaultLayout>
  )
}
