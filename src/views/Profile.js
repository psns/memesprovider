/* eslint-disable react-hooks/exhaustive-deps */
import {DefaultLayout} from '@/layouts'
import {Grid} from '@material-ui/core'
import {ProfileInformation, ProfileTab, Api} from '@/components'
import {AppContext} from '@/utils'
import {useContext, useState, useEffect} from 'react'
import {useParams} from 'react-router-dom'

const emptyUser = {
  id: 0,
  tag: '',
  username: '',
  avatar: false,
  background: false,
  firstName: '',
  lastName: '',
  description: '',
  credentialId: 0,
  createdAt: '2021-05-11T13:33:04.513Z',
  _count: {
    followers: 0,
    following: 0,
    followedTags: 0,
    posts: 0,
  },
}

export default function Profile() {
  const {user} = useContext(AppContext)
  const {id} = useParams()
  const [userInfo, setUserInfo] = useState(emptyUser)
  const [tabValue, setTabValue] = useState(0)
  const count = 25

  function loadProfileInfo() {
    Api.v1.profiles.getUnique(Number(id)).then(profile => setUserInfo(profile))
  }

  const [profilePosts, setProfilePosts] = useState([])
  const [pagePost, setPagePost] = useState(0)
  function loadProfilePosts(concat = false, pageOffset = 0) {
    Api.v1.profiles
      .getProfilePosts(id, {
        count,
        page: pagePost + pageOffset,
        requestor: user.id,
      })
      .then(posts =>
        setProfilePosts(concat ? profilePosts.concat(posts) : posts),
      )
  }

  const [profileLikes, setProfileLikes] = useState([])
  const [pageLikes, setPageLikes] = useState(0)
  function loadProfileLikes(concat = false, pageOffset = 0) {
    Api.v1.profiles
      .getProfileLikes(id, {
        count,
        page: pageLikes + pageOffset,
        requestor: user.id,
      })
      .then(posts =>
        setProfileLikes(concat ? profileLikes.concat(posts) : posts),
      )
  }

  function handleScroll(e) {
    const bottom =
      e.target.clientHeight === e.target.scrollHeight - e.target.scrollTop

    if (bottom) {
      if (tabValue === 0 && pagePost < 7) {
        loadProfilePosts(true, 1)
        setPagePost(pagePost + 1)
      } else if (tabValue === 1 && pageLikes < 7) {
        loadProfileLikes(true, 1)
        setPageLikes(pageLikes + 1)
      }
    }
  }

  useEffect(() => {
    loadProfileInfo()
    loadProfilePosts(false)
    loadProfileLikes(false)
  }, [id])

  return (
    <DefaultLayout>
      <Grid
        container
        direction="column"
        wrap="nowrap"
        spacing={0}
        style={{overflowY: 'auto', overflowX: 'hidden'}}
        onScroll={handleScroll}
      >
        <ProfileInformation
          userInfo={userInfo}
          onActionClicked={loadProfileInfo}
        />
        <ProfileTab
          posts={profilePosts}
          likes={profileLikes}
          value={tabValue}
          onChange={setTabValue}
        />
      </Grid>
    </DefaultLayout>
  )
}
