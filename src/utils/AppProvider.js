import {useState} from 'react'
import AppContext from './AppContext'

// Define an AppContext provider
export default function AppProvider({children}) {
  const [logged, setLogged] = useState(false)
  const [user, setUser] = useState(null)

  return (
    <AppContext.Provider
      value={{
        logged,
        setLogged,
        user,
        setUser,
      }}
    >
      {children}
    </AppContext.Provider>
  )
}
