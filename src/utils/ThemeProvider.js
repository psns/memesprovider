import {createMuiTheme, ThemeProvider as Theme} from '@material-ui/core/styles'

/**
 * Define a theme for the app
 */
const mprvdMainTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#352C44',
      light: '#352C44',
      dark: '#352C44',
      contrastText: '#fff',
    },
    success: {
      main: '#8AA248',
      light: '#8AA248',
      dark: '#8AA248',
    },
    error: {
      main: '#FF665B',
      light: '#FF665B',
      dark: '#FF665B',
    },
    background: {
      light: '#eee',
    },
  },
})

/**
 * Define a basic theme provider.
 * All children will inherit the defined theme above
 */
export default function ThemeProvider({children}) {
  return <Theme theme={mprvdMainTheme}>{children}</Theme>
}
