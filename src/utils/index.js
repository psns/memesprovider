import themeProvider from './ThemeProvider'
import appContext from './AppContext'
import appProvider from './AppProvider'

export const ThemeProvider = themeProvider
export const AppContext = appContext
export const AppProvider = appProvider
export const FindTagsInStr = (str, identifier = '#') => {
  let tags = []
  let load = false
  let currentTag = ''
  for (let i = 0; i < str.length; i++) {
    if (str[i] === identifier) load = true
    else if (str[i] === ' ') {
      if (currentTag !== '') tags.push(currentTag)
      load = false
      currentTag = ''
    } else if (i + 1 === str.length && load) {
      currentTag += str[i]
      tags.push(currentTag)
    } else if (load) currentTag += str[i]
  }
  return tags
}

export const MagnifyComment = (str, identifier = '#') => {
  let newStr = ''
  let load = false
  let currentTag = ''
  for (let i = 0; i < str.length; i++) {
    if (str[i] === identifier) load = true
    else if (str[i] === ' ') {
      newStr += str[i]
      if (currentTag !== '')
        newStr += `<span class="tag-style">#${currentTag}</span> `
      load = false
      currentTag = ''
    } else if (i + 1 === str.length && load) {
      currentTag += str[i]
      newStr += `<span class="tag-style">#${currentTag}</span> `
    } else if (load) currentTag += str[i]
    else newStr += str[i]
  }
  return newStr
}
