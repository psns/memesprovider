# V1 of Memes provider API

# Session 

All session information are stored into the `req.session.v1` object.

```js
{
    userId: Number, // user's id. Used to ensure access to the API 
}
```

# Controllers

## users

### `register`

Create a new user in the database and create a session 

```js
method: POST 
path: /api/v1/users
params: raw/JSON
param-value: {
    "email": String,        // required, email format
    "password": String,     // required, min-length: 10
    "tag": String,          // required, max-length: 30
    "username": String,     // required, max-length: 30
    "firstName": String,    // required, max-length: 30
    "lastName": String,     // required, max-length: 30
    "description": String   // required, max-length: 150
}
return: model.Profile({with: {followers: true, following: true}})
```


