const {request, response} = require('express')
const path = require('path')
const fs = require('fs-extra')
const {StatusCodes} = require('http-status-codes')
const postModel = require('../../../models/post')
const tagModel = require('../../../models/tag')
const {
  getProfileIdFromSession,
  castInDBMedia,
  getSessionFiles,
  clearSessionFiles,
  generateNewFilePath,
} = require('../utils')

/**
 * Return a list of posts. Can be filtered. See validator or model options.
 * @param {request} req
 * @param {response} res
 */
exports.list = (req, res) => {
  return postModel
    .list(req.body)
    .then(posts => res.status(StatusCodes.OK).send(posts))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * Return details about an unique post.
 * @param {request} req
 * @param {response} res
 * @returns
 */
exports.getUnique = (req, res) => {
  req.query.requestor = Number(req.query.requestor ?? -1)
  const id = parseInt(req.params.id)
  // Verify :id parameter
  if (isNaN(id)) return res.status(StatusCodes.BAD_REQUEST).send()

  return postModel
    .getUnique(id, req.query)
    .then(post => res.status(StatusCodes.OK).send(post))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * Create a post on database
 * @param {request} req
 * @param {response} res
 */
exports.create = async (req, res) => {
  const tags = req.body.tags ?? []
  const authorId = getProfileIdFromSession(req)

  if (!Array.isArray(tags))
    return res.status(StatusCodes.BAD_REQUEST).send('Invalid tags parameters')
  if (authorId === -1) return res.status(StatusCodes.UNAUTHORIZED).send()

  const medias = castInDBMedia(getSessionFiles(req))

  req.body.tags = await tagModel.createIfNotExist(tags)

  return postModel
    .create({authorId, medias, ...req.body})
    .then(post =>
      post.medias.forEach(media =>
        fs.move(media.tempUrl, generateNewFilePath(media.id)),
      ),
    )
    .then(() => clearSessionFiles(req))
    .then(() => res.status(StatusCodes.CREATED).send())
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * Delete a post.
 * @param {request} req
 * @param {response} res
 * @returns
 */
exports.delete = (req, res) => {
  const id = parseInt(req.params.id)
  // Verify :id parameter
  if (isNaN(id)) return res.status(StatusCodes.BAD_REQUEST).send()

  return postModel
    .delete(id, getProfileIdFromSession(req))
    .then(() => res.status(StatusCodes.OK).send())
    .catch(e => res.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * Return likes on a post.
 * @param {request} req
 * @param {response} res
 * @returns
 */
exports.getLikes = (req, res) => {
  req.query.count = Number(req.query.count ?? 25)
  req.query.page = Number(req.query.page ?? 0)

  const id = parseInt(req.params.id)
  // Verify :id parameter
  if (isNaN(id)) return res.status(StatusCodes.BAD_REQUEST).send()

  return postModel
    .getLikesOnPost(id, req.query)
    .then(profiles => res.status(StatusCodes.OK).send(profiles))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * Return replies on a post.
 * @param {request} req
 * @param {response} res
 * @returns
 */
exports.getReplies = (req, res) => {
  req.query.count = Number(req.query.count ?? 25)
  req.query.page = Number(req.query.page ?? 0)
  req.query.requestor = Number(req.query.requestor ?? -1)

  const id = parseInt(req.params.id)
  // Verify :id parameter
  if (isNaN(id)) return res.status(StatusCodes.BAD_REQUEST).send()

  return postModel
    .getRepliesOnPost(id, req.query)
    .then(profiles => res.status(StatusCodes.OK).send(profiles))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * Handles a like request.
 * @param {request} req
 * @param {response} res
 */
exports.likeRequest = (req, res) => {
  const postId = parseInt(req.params.id)
  const profileId = getProfileIdFromSession(req)
  // Verify :id parameter
  if (isNaN(postId)) return res.status(StatusCodes.BAD_REQUEST).send()
  if (profileId === -1) return res.status(StatusCodes.UNAUTHORIZED).send()

  postModel
    .like(postId, profileId)
    .then(created => res.status(StatusCodes.OK).send(created))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * Handles an unlike request.
 * @param {request} req
 * @param {response} res
 */
exports.unlikeRequest = (req, res) => {
  const postId = parseInt(req.params.id)
  const profileId = getProfileIdFromSession(req)
  // Verify :id parameter
  if (isNaN(postId)) return res.status(StatusCodes.BAD_REQUEST).send()
  if (profileId === -1) return res.status(StatusCodes.UNAUTHORIZED).send()

  postModel
    .unlike(postId, profileId)
    .then(created => res.status(StatusCodes.OK).send(created))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}
