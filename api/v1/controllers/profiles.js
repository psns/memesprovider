const {request, response} = require('express')
const fs = require('fs-extra')
const {StatusCodes} = require('http-status-codes')
const {
  createSession,
  clearSessionFiles,
  getProfileIdFromSession,
  getSessionAvatar,
  getSessionBackground,
  generateNewAvatarPath,
  generateNewBackgroundPath,
} = require('../utils')
const profile = require('../../../models/profile')
const postModel = require('../../../models/post')
const argon2 = require('argon2')

/**
 * Return a list of profile. Can be filtered. See validator or model options.
 * @param {request} req
 * @param {response} res
 */
exports.list = (req, res) => {
  req.query.count = Number(req.query.count ?? 25)
  req.query.page = Number(req.query.page ?? 0)
  return profile
    .list(req.query)
    .then(tags => res.status(StatusCodes.OK).send(tags))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 *  Register a new client in the database
 * @param {request} req
 * @param {response} res
 */
exports.register = (req, res) => {
  return argon2
    .hash(req.body.password)
    .then(hashPass => (req.body.password = hashPass))
    .then(() => profile.register(req.body))
    .then(({profile}) => createSession(req, profile.id))
    .then(profileId => profile.getProfile(profileId, true))
    .then(userProfile => res.status(StatusCodes.OK).send(userProfile))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * Return the user profile matching with given credential
 * @param {request} req
 * @param {response} res
 */
exports.login = async (req, res) => {
  const profileInfo = await profile.login(req.body)
  if (profileInfo == null) return res.status(StatusCodes.NOT_FOUND).send()

  const valid = await argon2.verify(profileInfo.password, req.body.password)

  if (!valid) return res.status(StatusCodes.UNAUTHORIZED).send()

  createSession(req, profileInfo.profile.id)

  return profile
    .getProfile(profileInfo.profile.id, true)
    .then(userProfile => res.status(StatusCodes.OK).send(userProfile))
    .catch(e => res.status(StatusCodes.BAD_REQUEST).send(e))
}

/**
 * Return profile info if the user is already logged.
 * @param {request} req
 * @param {result} res
 * @returns
 */
exports.logged = (req, res) => {
  return profile
    .getProfile(getProfileIdFromSession(req), true)
    .then(userProfile => res.status(StatusCodes.OK).send(userProfile))
    .catch(e => res.status(StatusCodes.BAD_REQUEST).send(e))
}

/**
 * Destroy the session.
 * @param {request} req
 * @param {response} res
 * @returns
 */
exports.logout = (req, res) => {
  return req.session.destroy(() => res.send(''))
}

/**
 * Return the profile of a given user's id
 * @param {request} req
 * @param {response} res
 */
exports.getProfile = (req, res) => {
  const id = parseInt(req.params.id)
  // Verify :id parameter
  if (isNaN(id)) return res.status(StatusCodes.BAD_REQUEST).send()

  return profile
    .getProfile(id, id === getProfileIdFromSession(req))
    .then(userProfile =>
      res
        .status(userProfile !== null ? StatusCodes.OK : StatusCodes.NOT_FOUND)
        .send(userProfile),
    )
    .catch(e => res.status(StatusCodes.BAD_REQUEST).send(e))
}

/**
 * Return followers of a profile.
 * @param {request} req
 * @param {response} res
 */
exports.getProfileFollowers = (req, res) => {
  req.query.count = Number(req.query.count ?? 25)
  req.query.page = Number(req.query.page ?? 0)

  const id = parseInt(req.params.id)
  // Verify :id parameter
  if (isNaN(id)) return res.status(StatusCodes.BAD_REQUEST).send()

  return profile
    .getProfileFollowers(id, req.query)
    .then(followers => res.status(StatusCodes.OK).send(followers))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * Return followed profiles of a profile.
 * @param {request} req
 * @param {response} res
 */
exports.getProfileFollowed = (req, res) => {
  req.query.count = Number(req.query.count ?? 25)
  req.query.page = Number(req.query.page ?? 0)
  req.query.inId = JSON.parse(req.query.inId ?? '[]')

  const id = parseInt(req.params.id)
  // Verify :id parameter
  if (isNaN(id)) return res.status(StatusCodes.BAD_REQUEST).send()

  return profile
    .getProfileFollowed(id, req.query)
    .then(followed => res.status(StatusCodes.OK).send(followed))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * Return followed profiles of a profile.
 * @param {request} req
 * @param {response} res
 */
exports.getProfilePosts = (req, res) => {
  req.query.count = Number(req.query.count ?? 25)
  req.query.page = Number(req.query.page ?? 0)
  req.query.requestor = getProfileIdFromSession(req)

  const id = parseInt(req.params.id)
  // Verify :id parameter
  if (isNaN(id)) return res.status(StatusCodes.BAD_REQUEST).send()

  return profile
    .getProfilePosts(id, req.query)
    .then(posts => res.status(StatusCodes.OK).send(posts))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * Return followed profiles of a profile.
 * @param {request} req
 * @param {response} res
 */
exports.getProfileLikes = (req, res) => {
  req.query.count = Number(req.query.count ?? 25)
  req.query.page = Number(req.query.page ?? 0)
  req.query.requestor = getProfileIdFromSession(req)

  const id = parseInt(req.params.id)
  // Verify :id parameter
  if (isNaN(id)) return res.status(StatusCodes.BAD_REQUEST).send()

  return profile
    .getProfileLikes(id, req.query)
    .then(posts => res.status(StatusCodes.OK).send(posts))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * Set info of a profile.
 * @param {request} req
 * @param {response} res
 */
exports.setProfile = async (req, res) => {
  const id = parseInt(req.params.id)
  const profileId = getProfileIdFromSession(req)
  // Verify :id parameter
  if (isNaN(id)) return res.status(StatusCodes.BAD_REQUEST).send()
  if (profileId === -1 || profileId !== id)
    return res.status(StatusCodes.UNAUTHORIZED).send()

  const avatar = getSessionAvatar(req)
  const background = getSessionBackground(req)

  if (avatar != null) {
    fs.removeSync(generateNewAvatarPath(profileId))
    fs.moveSync(avatar.tempFilePath, generateNewAvatarPath(profileId))
    req.body.avatar = true
  }

  if (background != null) {
    fs.removeSync(generateNewBackgroundPath(profileId))
    fs.moveSync(background.tempFilePath, generateNewBackgroundPath(profileId))
    req.body.background = true
  }

  clearSessionFiles(req)

  if (req.body.password != null) {
    req.body.password = await argon2.hash(req.body.password)
  }

  const {email, password} = req.body
  req.body.password = undefined
  req.body.email = undefined

  return profile
    .setProfileInfo(id, req.body, {email, password})
    .then(profile => res.status(StatusCodes.OK).send(profile))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * Return meme line of a profile.
 * @param {request} req
 * @param {response} res
 */
exports.getLine = (req, res) => {
  req.query.count = Number(req.query.count ?? 25)
  req.query.page = Number(req.query.page ?? 0)
  req.query.requestor = Number(req.query.requestor ?? -1)

  const id = parseInt(req.params.id)
  // Verify :id parameter
  if (isNaN(id)) return res.status(StatusCodes.BAD_REQUEST).send()

  return postModel
    .getMemeLine(id, req.query)
    .then(posts => res.status(StatusCodes.OK).send(posts))
    .catch(e => res.status.length(StatusCodes.INTERNAL_SERVER_ERROR).send())
}
