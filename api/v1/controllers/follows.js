const {request, response} = require('express')
const {StatusCodes} = require('http-status-codes')
const {getProfileIdFromSession} = require('../utils')
const followModel = require('../../../models/follow')

/**
 * Handles a follow request.
 * @param {request} req
 * @param {response} res
 */
exports.followRequest = (req, res) => {
  const followedId = parseInt(req.params.id)
  const profileId = getProfileIdFromSession(req)
  // Verify :id parameter
  if (isNaN(followedId)) return res.status(StatusCodes.BAD_REQUEST).send()
  if (profileId === -1) return res.status(StatusCodes.UNAUTHORIZED).send()
  if (profileId === followedId)
    return res.status(StatusCodes.BAD_REQUEST).send()

  followModel
    .follow(profileId, followedId)
    .then(created => res.status(StatusCodes.OK).send(created))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * Handle and unFollow request.
 * @param {request} req
 * @param {response} res
 * @returns
 */
exports.unFollowRequest = (req, res) => {
  const followedId = parseInt(req.params.id)
  const profileId = getProfileIdFromSession(req)

  // Verify :id parameter
  if (isNaN(followedId)) return res.status(StatusCodes.BAD_REQUEST).send()
  if (profileId === -1) return res.status(StatusCodes.UNAUTHORIZED).send()
  if (profileId === followedId)
    return res.status(StatusCodes.BAD_REQUEST).send()

  followModel
    .unFollow(profileId, followedId)
    .then(() => res.status(StatusCodes.OK).send())
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}
