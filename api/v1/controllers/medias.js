const {request, response} = require('express')
const fs = require('fs-extra')
const path = require('path')
const {StatusCodes} = require('http-status-codes')
const {
  addFileToSession,
  getSessionFiles,
  clearSessionFiles,
  addAvatarToSession,
  addBackgroundToSession,
} = require('../utils')

/**
 *
 * @param {request} req
 * @param {response} res
 * @returns
 */
exports.upload = (req, res) => {
  if (!req.files || Object.keys(req.files).length === 0)
    return res.status(StatusCodes.BAD_REQUEST).send('No files were uploaded.')

  const file = req.files['media']
  const avatar = req.files['avatar']
  const background = req.files['background']

  if (file != null) addFileToSession(req, file)
  if (avatar != null) addAvatarToSession(req, avatar)
  if (background != null) addBackgroundToSession(req, background)

  res.status(StatusCodes.OK).send(file)
}

/**
 * Clear all files presents in user's session
 * @param {request} req
 * @param {response} res
 * @returns
 */
exports.clearSessionFiles = (req, res) => {
  const files = getSessionFiles(req)
  files.forEach(file => fs.remove(path.join(process.cwd(), file.tempFilePath)))
  clearSessionFiles(req)
  return res.status(StatusCodes.OK).send()
}
