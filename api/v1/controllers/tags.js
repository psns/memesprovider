const {request, response} = require('express')
const {StatusCodes} = require('http-status-codes')
const tagModel = require('../../../models/tag')
const {getProfileIdFromSession} = require('../utils')

/**
 * Return information about a tag with the given id.
 * @param {request} req
 * @param {response} res
 * @returns
 */
exports.getUnique = (req, res) => {
  const id = parseInt(req.params.id)
  // Verify :id parameter
  if (isNaN(id)) return res.status(StatusCodes.BAD_REQUEST).send()

  return tagModel
    .getUnique(id)
    .then(tagInfo =>
      res
        .status(tagInfo !== null ? StatusCodes.OK : StatusCodes.NOT_FOUND)
        .send(tagInfo),
    )
    .catch(e => res.status(StatusCodes.BAD_REQUEST).send(e))
}

/**
 * Return a list of tag. Can be filtered. See validator or model options.
 * @param {request} req
 * @param {response} res
 */
exports.list = (req, res) => {
  req.query.count = Number(req.query.count ?? 25)
  req.query.page = Number(req.query.page ?? 0)

  return tagModel
    .list(req.query)
    .then(tags => res.status(StatusCodes.OK).send(tags))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * Create a tag in the database.
 * @param {request} req
 * @param {response} res
 * @returns
 */
exports.create = (req, res) => {
  return tagModel
    .createIfNotExist(req.body.tags)
    .then(tag => res.status(StatusCodes.CREATED).send(tag))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * When user wants to follow a tag.
 * @param {request} req
 * @param {response} res
 */
exports.follow = (req, res) => {
  const tagId = parseInt(req.params.id)
  const profileId = getProfileIdFromSession(req)
  // Verify :id parameter
  if (isNaN(tagId)) return res.status(StatusCodes.BAD_REQUEST).send()
  if (profileId === -1) return res.status(StatusCodes.UNAUTHORIZED).send()

  return tagModel
    .follow(profileId, tagId)
    .then(() => res.status(StatusCodes.OK).send())
    .catch(e => res.status(StatusCodes.BAD_REQUEST).send())
}

/**
 * When user wants to unFollow a tag.
 * @param {request} req
 * @param {response} res
 */
exports.unFollow = (req, res) => {
  const tagId = parseInt(req.params.id)
  const profileId = getProfileIdFromSession(req)
  // Verify :id parameter
  if (isNaN(tagId)) return res.status(StatusCodes.BAD_REQUEST).send()
  if (profileId === -1) return res.status(StatusCodes.UNAUTHORIZED).send()

  return tagModel
    .unFollow(profileId, tagId)
    .then(() => res.status(StatusCodes.OK).send())
    .catch(e => res.status(StatusCodes.BAD_REQUEST).send())
}

/**
 * Return followers of a tag.
 * @param {request} req
 * @param {response} res
 */
exports.getTagFollowers = (req, res) => {
  req.query.count = Number(req.query.count ?? 25)
  req.query.page = Number(req.query.page ?? 0)

  const id = parseInt(req.params.id)
  // Verify :id parameter
  if (isNaN(id)) return res.status(StatusCodes.BAD_REQUEST).send()

  return tagModel
    .getTagFollowers(id, req.query)
    .then(followers => res.status(StatusCodes.OK).send(followers))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}

/**
 * Return posts of a tag.
 * @param {request} req
 * @param {response} res
 */
exports.getTagPosts = (req, res) => {
  const id = parseInt(req.params.id)

  req.query.count = Number(req.query.count ?? 25)
  req.query.page = Number(req.query.page ?? 0)
  req.query.requestor = getProfileIdFromSession(req)
  if (isNaN(id)) return res.status(StatusCodes.BAD_REQUEST).send()

  return tagModel
    .getTagPosts(id, req.query)
    .then(posts => res.status(StatusCodes.OK).send(posts))
    .catch(e => res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e))
}
