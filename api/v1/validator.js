const {request, response, next} = require('express')
const {body, query, validationResult} = require('express-validator')

module.exports = {
  /**
   * Middleware which check if no error occurred during validation process
   * @param {request} req
   * @param {response} res
   * @param {next} next
   * @returns
   */
  check: (req, res, next) => {
    const errors = validationResult(req)
    return errors.isEmpty()
      ? next()
      : res.status(400).json({errors: errors.array()})
  },

  // Validator rules
  rules: {
    profiles: {
      list: [
        query('count').isInt({min: 1, max: 50}).optional(),
        query('page').isInt({min: 0}).not().isString().optional(),
        query('tag').isString().optional(),
        query('username').isString().optional(),
      ],
      getProfileFollowed: [
        query('count').isInt({min: 1, max: 50}).optional(),
        query('page').isInt({min: 0}).not().isString().optional(),
        query('tag').isString().optional(),
        query('username').isString().optional(),
        query('inId').isString().optional(),
      ],
      getProfilePosts: [
        query('count').isInt({min: 1, max: 50}).optional(),
        query('page').isInt({min: 0}).optional(),
        query('comment').isString().optional(),
      ],
      register: [
        body('email').isEmail(),
        body('password').isLength({min: 10}),
        body('tag')
          .notEmpty()
          .isLength({max: 30})
          .custom(value => /^\S*$/.test(value)),
        body('username').notEmpty().isLength({max: 30}),
        body('firstName').notEmpty().isLength({max: 30}),
        body('lastName').notEmpty().isLength({max: 30}),
        body('description').isLength({max: 150}),
      ],
      set: [
        body('email').isEmail().optional(),
        body('password').isLength({min: 10}).optional(),
        body('tag')
          .notEmpty()
          .isLength({max: 30})
          .custom(value => /^\S*$/.test(value))
          .optional(),
        body('username').notEmpty().isLength({max: 30}).optional(),
        body('firstName').notEmpty().isLength({max: 30}).optional(),
        body('lastName').notEmpty().isLength({max: 30}).optional(),
        body('description').isLength({max: 150}).optional(),
        body('avatar').isBoolean().optional(),
        body('description').isString().optional(),
      ],
      login: [body('email').notEmpty(), body('password').notEmpty()],
    },
    tags: {
      list: [
        query('count').isInt({min: 1, max: 50}).optional(),
        query('page').isInt({min: 0}).optional(),
        query('name').isString().optional(),
      ],
      create: [],
    },
    posts: {
      getUnique: [query('requestor').isInt().optional()],
      create: [
        body('comment').isString().notEmpty(),
        body('privatePost').isBoolean().notEmpty(),
        body('parentId').isInt().not().isString().optional(),
        // body('tags') is also necessary but it can't be validate with express
        // validator
      ],
      getReplies: [
        query('count').isInt({min: 1, max: 50}).optional(),
        query('page').isInt({min: 0}).optional(),
      ],
    },
  },
}
