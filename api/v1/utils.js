const {request} = require('express')
const path = require('path')
const {StatusCodes} = require('http-status-codes')

/**
 * Middleware to ensures that the client is logged
 * @param {request} req
 * @param {response} res
 * @param {next} next
 */
exports.authRequired = (req, res, next) => {
  if (req.session.v1?.logged) return next()
  return res.status(StatusCodes.UNAUTHORIZED).send()
}

/**
 * Create a default api/v1 session.
 * @param {request} req
 * @param {Number} userId
 * @returns
 */
exports.createSession = (req, profileId) => {
  req.session.v1 = {
    avatar: null,
    background: null,
    logged: true,
    profileId,
    files: [],
  }
  return profileId
}

/**
 * Used to protect certain resources.
 * eg: For a follow request, user A can make a request to follow user B
 * But he can't ask B to follow C.
 * @param {request} req
 * @param {Number} profileId
 * @returns true if the user make a request for himself.
 */
exports.isSelf = (req, profileId) => {
  return req.session.v1?.profileId ?? -1 === profileId
}

/**
 * Return the profile id stored in the user's session.
 * @param {request} req
 */
exports.getProfileIdFromSession = req => {
  return req.session.v1?.profileId ?? -1
}

/**
 * Add a file to the user session.
 * @param {request} req
 * @param {} file
 */
exports.addFileToSession = (req, file) => {
  req.session.v1.files.push(file)
}

/**
 * Add an avatar to the user session.
 * @param {request} req
 * @param {} file
 */
exports.addAvatarToSession = (req, avatar) => {
  req.session.v1.avatar = avatar
}

/**
 * Add a background to the user session.
 * @param {request} req
 * @param {} file
 */
exports.addBackgroundToSession = (req, background) => {
  req.session.v1.background = background
}

/**
 * Return file stored in user's session.
 * @param {request} req
 * @returns {Array>}
 */
exports.getSessionFiles = req => {
  return req.session.v1?.files ?? []
}

/**
 * Return session avatar.
 * @param {request} req
 * @returns {Array>}
 */
exports.getSessionAvatar = req => {
  return req.session.v1?.avatar
}

/**
 * Return session background.
 * @param {request} req
 * @returns {Array>}
 */
exports.getSessionBackground = req => {
  return req.session.v1?.background
}

/**
 * Delete all files from user's session.
 * @param {request} req
 */
exports.clearSessionFiles = req => {
  req.session.v1.files = []
  req.session.v1.avatar = null
  req.session.v1.background = null
}

/**
 * Cast en array of session file into an array of Media.
 * @param {Number} postId
 * @param {Array} files
 * @returns
 */
exports.castInDBMedia = files => {
  return files.map(({mimetype, md5, encoding, size, tempFilePath}) => ({
    mimetype,
    md5,
    encoding,
    size,
    tempUrl: tempFilePath,
  }))
}

/**
 *  @param {Number} mediaId
 */
exports.generateNewFilePath = mediaId => {
  return path.join('uploads/v1/perm/', `${mediaId}`)
}

/**
 *  @param {Number} id
 */
exports.generateNewAvatarPath = id => {
  return path.join('uploads/v1/perm/avatar', `${id}`)
}

/**
 *  @param {Number} id
 */
exports.generateNewBackgroundPath = id => {
  return path.join('uploads/v1/perm/background', `${id}`)
}
