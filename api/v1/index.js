module.exports = {
  utils: require('./utils'),
  validator: require('./validator'),
  controllers: {
    profiles: require('./controllers/profiles'),
    follows: require('./controllers/follows'),
    tags: require('./controllers/tags'),
    medias: require('./controllers/medias'),
    posts: require('./controllers/posts'),
  },
}
