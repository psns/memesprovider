const {request, response, next, Router, static} = require('express')
const fs = require('fs-extra')
const fileUpload = require('express-fileupload')
const v1 = require('./v1')

/**
 * Setup the router for the version 1 of the api.
 * @return {Router} configured router
 */
exports.setupV1Router = () => {
  const api = Router()

  /////////// Auth routes ///////////
  api.post(
    '/signin',
    v1.validator.rules.profiles.login,
    v1.validator.check,
    v1.controllers.profiles.login,
  )
  api.post(
    '/signup',
    v1.validator.rules.profiles.register,
    v1.validator.check,
    v1.controllers.profiles.register,
  )
  api.get('/logged', v1.utils.authRequired, v1.controllers.profiles.logged)
  api.get('/logout', v1.utils.authRequired, v1.controllers.profiles.logout)

  /////////// Profiles controllers ///////////
  api.use('/profiles', v1.utils.authRequired)
  api.use('/profiles/*', v1.utils.authRequired)

  api.get(
    '/profiles',
    v1.validator.rules.profiles.list,
    v1.validator.check,
    v1.controllers.profiles.list,
  )
  api.get('/profiles/:id/', v1.controllers.profiles.getProfile)
  api.put(
    '/profiles/:id/',
    v1.validator.rules.profiles.set,
    v1.validator.check,
    v1.controllers.profiles.setProfile,
  )
  api.get(
    '/profiles/:id/followers',
    v1.validator.rules.profiles.list,
    v1.validator.check,
    v1.controllers.profiles.getProfileFollowers,
  )
  api.get(
    '/profiles/:id/followed',
    v1.validator.rules.profiles.getProfileFollowed,
    v1.validator.check,
    v1.controllers.profiles.getProfileFollowed,
  )
  api.get(
    '/profiles/:id/posts/',
    v1.validator.rules.profiles.getProfilePosts,
    v1.validator.check,
    v1.controllers.profiles.getProfilePosts,
  )
  api.get(
    '/profiles/:id/likes',
    v1.validator.rules.profiles.getProfilePosts, // Not an error, same as /profiles/:id/posts
    v1.validator.check,
    v1.controllers.profiles.getProfileLikes,
  )
  api.get(
    '/profiles/:id/line',
    v1.validator.rules.posts.getReplies, // Not an error, same as /profiles/:id/posts
    v1.validator.check,
    v1.controllers.profiles.getLine,
  )

  /////////// Follows controllers ///////////
  api.use('/follows/:id', v1.utils.authRequired)

  api.post('/follows/:id', v1.controllers.follows.followRequest)
  api.delete('/follows/:id', v1.controllers.follows.unFollowRequest)

  /////////// Tags controllers ///////////
  api.use('/tags', v1.utils.authRequired)
  api.use('/tags/*', v1.utils.authRequired)

  api.get(
    '/tags',
    v1.validator.rules.tags.list,
    v1.validator.check,
    v1.controllers.tags.list,
  )
  api.get('/tags/:id', v1.controllers.tags.getUnique)
  api.get(
    '/tags/:id/followers',
    v1.validator.rules.profiles.list, // Not an error, same as /profiles/:id/followers
    v1.validator.check,
    v1.controllers.tags.getTagFollowers,
  )
  api.get(
    '/tags/:id/posts',
    v1.validator.rules.profiles.getProfilePosts, // Not an error, same as /profiles/:id/posts
    v1.validator.check,
    v1.controllers.tags.getTagPosts,
  )
  api.post(
    '/tags',
    v1.validator.rules.tags.create,
    v1.validator.check,
    v1.controllers.tags.create,
  )
  api.post('/tags/:id/follow', v1.controllers.tags.follow)
  api.delete('/tags/:id/follow', v1.controllers.tags.unFollow)

  /////////// medias controllers ///////////
  // Delete temp files on each start
  fs.removeSync('./uploads/v1/tmp')
  fs.mkdirpSync('./uploads/v1/perm')
  fs.mkdirpSync('./uploads/v1/perm/avatar')
  fs.mkdirpSync('./uploads/v1/perm/background')
  api.use('/uploads', static('uploads'))
  api.use('/medias', v1.utils.authRequired)
  api.use(
    '/medias',
    fileUpload({
      createParentPath: true,
      useTempFiles: true,
      tempFileDir: './uploads/v1/tmp',
    }),
  )

  api.post('/medias', v1.controllers.medias.upload)
  api.delete('/medias', v1.controllers.medias.clearSessionFiles)

  /////////// posts controllers ///////////

  api.use('/posts', v1.utils.authRequired)
  api.use('/posts/*', v1.utils.authRequired)

  api.get(
    '/posts',
    v1.validator.rules.profiles.getProfilePosts, // Not an error, same as /profiles/:id/posts
    v1.validator.check,
    v1.controllers.posts.list,
  )
  api.get(
    '/posts/:id',
    v1.validator.rules.posts.getUnique, // Not an error, same as /profiles/:id/posts
    v1.validator.check,
    v1.controllers.posts.getUnique,
  )
  api.delete('/posts/:id', v1.controllers.posts.delete)
  api.get(
    '/posts/:id/likes',
    v1.validator.rules.profiles.list, // Not an error, same as /profiles/:id/followers
    v1.validator.check,
    v1.controllers.posts.getLikes,
  )
  api.get(
    '/posts/:id/replies',
    v1.validator.rules.posts.getReplies,
    v1.validator.check,
    v1.controllers.posts.getReplies,
  )
  api.post('/posts/:id/like', v1.controllers.posts.likeRequest)
  api.delete('/posts/:id/like', v1.controllers.posts.unlikeRequest)
  api.post(
    '/posts',
    v1.validator.rules.posts.create,
    v1.validator.check,
    v1.controllers.posts.create,
  )

  return api
}
