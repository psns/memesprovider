# Memes Provider (MPRVD)

### Content

This repo contains :

- Backend server (Node+Express)
- Frontend app (React)
- Postgres Bdd (docker-compose + Prisma)

### Required dependencies

- npm >= v7.10
- nodejs = LTS 14.16.X
- docker >= v20.10
- docker-compose >= v1.29

# Quick start

Start by installing repo dependencies

```shell
npm install
```

## Start DB

First, your docker daemon must be started (`sudo dockerd`)

```
docker-compose up
```

> If you starting the database for the first time, you must deploy it.
>
> ```
> npm run dbdeploy
> ```
> You can also seed it with example data.
> <b>Don't forget to extract `upload_demo.zip` at project's root.</b> Otherwise you'll not be able to see profile's avatar and background. The extracted folder contains static images.
>
>```
> npm run dbseed
>```
>
> You can explore data in database
>
> ```
> npm run dbexplore
> ```

## Set up for development

First start react app watcher

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

```
npm start
```

And in an other bash instance start the api

```
npm run api
```

## Deploy

```
npm run build
npm run deploy
```
