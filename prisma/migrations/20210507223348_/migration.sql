/*
  Warnings:

  - You are about to alter the column `password` on the `Credential` table. The data in that column could be lost. The data in that column will be cast from `Text` to `VarChar(95)`.
  - The primary key for the `Follow` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `id` on the `Follow` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Credential" ALTER COLUMN "password" SET DATA TYPE VARCHAR(95);

-- AlterTable
ALTER TABLE "Follow" DROP CONSTRAINT "Follow_pkey",
DROP COLUMN "id",
ADD PRIMARY KEY ("followerId", "followedId");
