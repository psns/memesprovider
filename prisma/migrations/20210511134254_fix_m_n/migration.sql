/*
  Warnings:

  - You are about to drop the `_PostToTag` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_ProfileToTag` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "_PostToTag" DROP CONSTRAINT "_PostToTag_A_fkey";

-- DropForeignKey
ALTER TABLE "_PostToTag" DROP CONSTRAINT "_PostToTag_B_fkey";

-- DropForeignKey
ALTER TABLE "_ProfileToTag" DROP CONSTRAINT "_ProfileToTag_A_fkey";

-- DropForeignKey
ALTER TABLE "_ProfileToTag" DROP CONSTRAINT "_ProfileToTag_B_fkey";

-- DropTable
DROP TABLE "_PostToTag";

-- DropTable
DROP TABLE "_ProfileToTag";
