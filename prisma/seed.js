const {PrismaClient} = require('@prisma/client')
const argon2 = require('argon2')

const prisma = new PrismaClient()

async function main() {
  const defaultMdp = await argon2.hash('1234567890')

  const dev = await prisma.profile.create({
    data: {
      tag: 'dev',
      username: 'DevAccount',
      firstName: 'David',
      lastName: 'Dupond',
      description: 'MPRVD Dev account',
      avatar: true,
      background: true,
      credential: {
        create: {
          email: 'dev@dev.fr',
          password: defaultMdp,
        },
      },
    },
  })

  const nicolas = await prisma.profile.create({
    data: {
      tag: 'nicolasdscp',
      username: 'Nicolas',
      firstName: 'Nicolas',
      lastName: 'Descamps',
      description: '',
      avatar: true,
      background: true,
      credential: {
        create: {
          email: 'nicolas@gmail.com',
          password: defaultMdp,
        },
      },
    },
  })

  const antonin = await prisma.profile.create({
    data: {
      tag: 'antototonin',
      username: 'Anto',
      firstName: 'Antonin',
      lastName: 'Behague',
      description: 'La wati pote',
      avatar: true,
      background: true,
      credential: {
        create: {
          email: 'antonin@gmail.com',
          password: defaultMdp,
        },
      },
    },
  })

  const louis = await prisma.profile.create({
    data: {
      tag: 'louis',
      username: 'Louis',
      firstName: 'Louis',
      lastName: 'Woisel',
      description: 'La wati pote 2',
      credential: {
        create: {
          email: 'louis@gmail.com',
          password: defaultMdp,
        },
      },
    },
  })

  console.log([dev, nicolas, antonin, louis])

  await prisma.follow.createMany({
    data: [
      // dev followers
      {followedId: 1, followerId: 2},
      {followedId: 1, followerId: 3},
      {followedId: 1, followerId: 4},
      // nicolas followers
      {followedId: 2, followerId: 3},
      {followedId: 2, followerId: 4},
      // antonin followers
      {followedId: 3, followerId: 2},
      {followedId: 3, followerId: 4},
      // louis followers
      {followedId: 4, followerId: 2},
      {followedId: 4, followerId: 3},
    ],
  })

  await prisma.post.create({
    data: {
      authorId: 1,
      comment: '#first posts',
      likes: {
        createMany: {
          data: [{profileId: 2}, {profileId: 3}, {profileId: 4}],
        },
      },
      tags: {
        create: {
          tag: {
            create: {
              name: 'first',
            },
          },
        },
      },
    },
  })

  await prisma.post.create({
    data: {
      authorId: 2,
      comment: '#first posts',
      likes: {
        createMany: {
          data: [{profileId: 3}, {profileId: 4}],
        },
      },
      tags: {
        create: [
          {
            tagId: 1,
          },
        ],
      },
    },
  })

  await prisma.post.create({
    data: {
      authorId: 3,
      comment: '#first posts',
      likes: {
        createMany: {
          data: [{profileId: 2}, {profileId: 4}],
        },
      },
      tags: {
        create: [
          {
            tagId: 1,
          },
        ],
      },
    },
  })

  await prisma.post.create({
    data: {
      authorId: 4,
      comment: '#first posts',
      likes: {
        createMany: {
          data: [{profileId: 2}, {profileId: 3}],
        },
      },
      tags: {
        create: [
          {
            tagId: 1,
          },
        ],
      },
    },
  })
}

main()
  .catch(e => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
