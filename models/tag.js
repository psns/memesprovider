const client = require('../prisma/client')

/**
 * Return information about a given tag id.
 * @param {Object} options filters the result
 * @returns
 */
exports.getUnique = id => {
  return client.tag.findUnique({
    where: {id},
    include: {
      _count: {
        select: {
          posts: true,
          followers: true,
        },
      },
    },
  })
}

/**
 * Return a list of tag which can be filtered with options.
 * @param {Object} options query options
 * @returns
 */
exports.list = ({count = 25, page = 0, name = ''}) => {
  return client.tag.findMany({
    skip: count * page,
    take: count,
    orderBy: {
      createdAt: 'asc',
    },
    where: {
      name: {
        contains: name,
      },
    },
    include: {
      _count: {
        select: {
          posts: true,
          followers: true,
        },
      },
    },
  })
}

/**
 * Create a tag.
 * @param {String} name of the tag
 * @returns
 */
exports.create = name => {
  return client.tag.create({
    data: {name},
  })
}

/**
 * Create a list of tag if they no exist.
 * @param {Array<String>} tags
 * @returns
 */
exports.createIfNotExist = tags => {
  return client.tag
    .createMany({
      data: tags.map(name => ({name})),
      skipDuplicates: true,
    })
    .then(() =>
      client.tag.findMany({
        where: {
          name: {
            in: tags,
          },
        },
      }),
    )
}

/**
 * Makes the profile following the tag.
 * @param {Number} profileId
 * @param {Number} tagId
 * @returns
 */
exports.follow = (profileId, tagId) => {
  return client.profilesOnTag.create({
    data: {
      profileId,
      tagId,
    },
  })
}

/**
 * Makes the profile unFollowing the tag.
 * @param {Number} profileId
 * @param {Number} tagId
 * @returns
 */
exports.unFollow = (profileId, tagId) => {
  return client.profilesOnTag.delete({
    where: {
      profileId_tagId: {
        profileId,
        tagId,
      },
    },
  })
}

/**
 * Return followers of a tag.
 * @param {Number} followedId
 * @param {Object} options
 * @returns
 */
exports.getTagFollowers = (
  tagId,
  {count = 25, page = 0, tag = '', username = ''},
) => {
  return client.profilesOnTag.findMany({
    where: {
      tagId,
      profile: {
        tag: {
          contains: tag,
        },
        username: {
          contains: username,
        },
      },
    },
    include: {
      profile: true,
    },
    skip: page * count,
    take: count,
    orderBy: {
      createdAt: 'desc',
    },
  })
}

/**
 * Return posts of a profile.
 * @param {Number} authorId
 * @param {Object} options
 * @returns
 */
exports.getTagPosts = (
  tagId,
  {count = 25, page = 0, comment = '', requestor = -1},
) => {
  return client.post.findMany({
    where: {
      AND: [
        {
          comment: {
            contains: comment,
          },
          tags: {
            some: {
              tagId: {
                equals: tagId,
              },
            },
          },
        },
        {
          OR: [
            {
              private: false,
            },
            {
              authorId: requestor,
            },
            {
              author: {
                following: {
                  some: {
                    followedId: {
                      equals: requestor,
                    },
                  },
                },
              },
            },
          ],
        },
      ],
    },
    select: {
      id: true,
    },
    skip: page * count,
    take: count,
    orderBy: {
      createdAt: 'desc',
    },
  })
}
