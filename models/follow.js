const client = require('../prisma/client')

/**
 * Make the followerId follows the followedId.
 * @param {Number} followerId profileId of the follower
 * @param {Number} followedId profileId of the followed
 * @returns
 */
exports.follow = (followerId, followedId) => {
  return client.follow.create({
    data: {followerId, followedId},
  })
}

/**
 * Delete the link between the followerId and the followedId.
 * @param {Number} followerId profileId of the follower
 * @param {Number} followedId profileId of the followed
 * @returns
 */
exports.unFollow = (followerId, followedId) => {
  return client.follow.delete({
    where: {
      followerId_followedId: {
        followedId,
        followerId,
      },
    },
  })
}
