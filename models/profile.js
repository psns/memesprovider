const client = require('../prisma/client')

/**
 * Return a list of profiles which can be filtered with options.
 * @param {Object} options query options
 * @returns
 */
exports.list = ({count = 25, page = 0, tag = ''}) => {
  return client.profile.findMany({
    skip: count * page,
    take: count,
    where: {
      tag: {
        contains: tag,
      },
    },
    include: {
      _count: {
        select: {
          followers: true,
          following: true,
          followedTags: true,
          posts: true,
        },
      },
    },
    orderBy: {
      createdAt: 'desc',
    },
  })
}

/**
 * Create a new user in the database
 * @param {Object} - User's info
 * @returns
 */
exports.register = ({
  email,
  password,
  tag,
  username,
  firstName,
  lastName,
  description,
}) => {
  return client.credential.create({
    data: {
      email,
      password,
      profile: {
        create: {
          tag,
          username,
          firstName,
          lastName,
          description,
        },
      },
    },
    include: {
      profile: true,
    },
  })
}

/**
 * Try to get user info from the DB with given credentials
 * @param {Object} - User's credentials
 */
exports.login = ({email}) => {
  return client.credential.findFirst({
    where: {email},
    include: {
      profile: true,
    },
  })
}

/**
 * Return a user's profile matching with the given user's id
 * @param {Number} userId id of the user's wanted profile
 * @returns
 */
exports.getProfile = (id, email) => {
  const req = {
    where: {id},
    include: {
      _count: {
        select: {
          followers: true,
          following: true,
          followedTags: true,
          posts: true,
        },
      },
    },
  }
  if (email)
    req.include.credential = {
      select: {
        email,
      },
    }

  return client.profile.findFirst(req)
}

/**
 * Return followers of a profile.
 * @param {Number} followedId
 * @param {Object} options
 * @returns
 */
exports.getProfileFollowers = (
  followedId,
  {count = 25, page = 0, tag = '', username = ''},
) => {
  return client.follow.findMany({
    orderBy: {
      followedAt: 'asc',
    },
    where: {
      followedId,
      follower: {
        tag: {
          contains: tag,
        },
        username: {
          contains: username,
        },
      },
    },
    skip: count * page,
    take: count,
    include: {
      follower: true,
    },
  })
}

/**
 * Return followed profiles of a profile.
 * @param {Number} followedId
 * @param {*} options
 * @returns
 */
exports.getProfileFollowed = (
  followerId,
  {count = 25, page = 0, tag = '', username = '', inId = []},
) => {
  const req = {
    orderBy: {
      followedAt: 'asc',
    },
    where: {
      followerId,
      followed: {
        tag: {
          contains: tag,
        },
        username: {
          contains: username,
        },
      },
    },
    skip: count * page,
    take: count,
    include: {
      followed: true,
    },
  }

  if (inId.length !== 0) req.where.followed.id = {in: inId}

  return client.follow.findMany(req)
}

/**
 * Return posts of a profile.
 * @param {Number} profileId
 * @param {Object} options
 * @returns
 */
exports.getProfilePosts = (
  profileId,
  {count = 25, page = 0, comment = '', requestor = profileId},
) => {
  return client.post.findMany({
    where: {
      AND: [
        {
          authorId: profileId,
          comment: {
            contains: comment,
          },
        },
        {
          OR: [
            {
              private: false,
            },
            {
              authorId: requestor,
            },
            {
              author: {
                following: {
                  some: {
                    followedId: {
                      equals: requestor,
                    },
                  },
                },
              },
            },
          ],
        },
      ],
    },
    select: {
      id: true,
    },
    skip: count * page,
    take: count,
    orderBy: {
      createdAt: 'desc',
    },
  })
}

/**
 * Returns liked posts.
 * @param {Number} profileId
 * @param {Object} options
 * @returns
 */
exports.getProfileLikes = (
  profileId,
  {count = 25, page = 0, comment = '', requestor = profileId},
) => {
  return client.post.findMany({
    where: {
      AND: [
        {
          comment: {
            contains: comment,
          },
          likes: {
            some: {
              profileId: {
                equals: profileId,
              },
            },
          },
        },
        {
          OR: [
            {
              private: false,
            },
            {
              authorId: requestor,
            },
            {
              author: {
                following: {
                  some: {
                    followedId: {
                      equals: requestor,
                    },
                  },
                },
              },
            },
          ],
        },
      ],
    },
    select: {
      id: true,
    },
    skip: count * page,
    take: count,
    orderBy: {
      createdAt: 'desc',
    },
  })
}

/**
 * Set info of a profile
 * @param {Number} id Id of the profile
 * @param {Object} info
 * @returns
 */
exports.setProfileInfo = (id, info, cred) => {
  return client.profile.update({
    where: {id},
    data: {
      ...info,
      credential: {
        update: cred,
      },
    },
    include: {
      credential: {
        select: {
          email: true,
        },
      },
      _count: {
        select: {
          followers: true,
          following: true,
          followedTags: true,
          posts: true,
        },
      },
    },
  })
}
