const client = require('../prisma/client')

/**
 * List posts.
 * @param {Object} options
 * @returns
 */
exports.list = ({count = 25, page = 0, comment = ''}) => {
  return client.post.findMany({
    skip: count * page,
    take: count,
    where: {
      comment: {
        contains: comment,
      },
    },
    include: {
      medias: true,
      author: true,
      tags: {
        include: {
          tag: true,
        },
      },
      _count: {
        select: {
          replies: true,
          likes: true,
        },
      },
    },
    orderBy: {
      createdAt: 'desc',
    },
  })
}

/**
 * Get detailed info about an unique post.
 * @param {Number} id
 * @param {Object} options
 * @returns
 */
exports.getUnique = (id, {requestor = -1}) => {
  let req = {
    where: {id},
    include: {
      medias: true,
      author: true,
      tags: {
        include: {
          tag: true,
        },
      },
      _count: {
        select: {
          replies: true,
          likes: true,
        },
      },
    },
  }

  if (requestor !== -1)
    req.include.likes = {
      select: {
        createdAt: true,
      },
      where: {
        profileId: {
          equals: requestor,
        },
      },
    }

  return client.post.findUnique(req)
}

/**
 * Create a post in database.
 * @param {Object} Options
 * @returns
 */
exports.create = ({
  authorId,
  comment,
  privatePost,
  parentId,
  medias,
  tags = [],
}) => {
  return client.post.create({
    data: {
      authorId,
      comment,
      parentId,
      private: privatePost,
      medias: {
        createMany: {
          data: medias,
        },
      },
      tags: {
        createMany: {
          data: tags.map(tag => ({tagId: tag.id})),
        },
      },
    },
    include: {
      medias: true,
    },
  })
}

/**
 * Return profile which likes the post.
 * @param {Number} postId
 * @param {Object} options
 * @returns
 */
exports.getLikesOnPost = (
  postId,
  {count = 25, page = 0, tag = '', username = ''},
) => {
  return client.likesOnPost.findMany({
    where: {
      postId,
      profile: {
        tag: {
          contains: tag,
        },
        username: {
          contains: username,
        },
      },
    },
    include: {
      profile: true,
    },
    skip: page * count,
    take: count,
    orderBy: {
      createdAt: 'desc',
    },
  })
}

/**
 * Return replies on a post.
 * @param {Number} parentId
 * @param {Object} options
 * @returns
 */
exports.getRepliesOnPost = (
  parentId,
  {count = 25, page = 0, requestor = -1},
) => {
  return client.post.findMany({
    where: {
      AND: [
        {
          parentId,
        },
        {
          OR: [
            {
              private: false,
            },
            {
              authorId: requestor,
            },
            {
              author: {
                following: {
                  some: {
                    followedId: {
                      equals: requestor,
                    },
                  },
                },
              },
            },
          ],
        },
      ],
    },
    select: {
      id: true,
    },
    skip: page * count,
    take: count,
    orderBy: {
      createdAt: 'desc',
    },
  })
}

/**
 * Makes the profileId like the postId
 * @param {Number} postId
 * @param {Number} profileId
 * @returns
 */
exports.like = (postId, profileId) => {
  return client.likesOnPost.create({
    data: {profileId, postId},
  })
}

/**
 * Makes the profileId unlike the postId
 * @param {Number} postId
 * @param {Number} profileId
 * @returns
 */
exports.unlike = (postId, profileId) => {
  return client.likesOnPost.delete({
    where: {
      profileId_postId: {
        profileId,
        postId,
      },
    },
  })
}

/**
 * Return meme line of a profile.
 * @param {Number} profileId
 * @param {Object} options
 * @returns
 */
exports.getMemeLine = (
  profileId,
  {count = 25, page = 0, requestor = profileId},
) => {
  return client.post.findMany({
    where: {
      AND: [
        {
          OR: [
            {
              author: {
                followers: {
                  some: {
                    followerId: {
                      equals: profileId,
                    },
                  },
                },
              },
            },
            {
              authorId: profileId,
            },
            {
              tags: {
                some: {
                  tag: {
                    followers: {
                      some: {
                        profileId: {
                          equals: profileId,
                        },
                      },
                    },
                  },
                },
              },
            },
          ],
        },
        {
          OR: [
            {
              private: false,
            },
            {
              authorId: requestor,
            },
            {
              author: {
                following: {
                  some: {
                    followedId: {
                      equals: requestor,
                    },
                  },
                },
              },
            },
          ],
        },
      ],
    },
    select: {
      id: true,
    },
    skip: page * count,
    take: count,
    orderBy: {
      createdAt: 'desc',
    },
  })
}

/**
 * Delete a post from the database.
 * @param {Number} postId
 * @returns
 */
exports.delete = (postId, authorId) => {
  const likes = client.likesOnPost.deleteMany({
    where: {postId, post: {authorId}},
  })
  const media = client.media.deleteMany({where: {postId, post: {authorId}}})
  const tags = client.tagsOnPost.deleteMany({where: {postId, post: {authorId}}})
  const post = client.post.deleteMany({where: {id: postId, authorId}})

  return client.$transaction([likes, media, tags, post])
}
