const express = require('express')
const fs = require('fs-extra')
const path = require('path')
const helmet = require('helmet')
const session = require('express-session')
const {setupV1Router} = require('./api/router')
const app = express()
const port = 8080

app.use(helmet())
app.use(
  session({
    secret: 'ed735d55415bee976b771989be8f7005',
    saveUninitialized: true,
    resave: false,
    cookie: {
      maxAge: null,
    },
  }),
)
app.use(express.json())
app.use('/api/v1', setupV1Router())

try {
  fs.statSync(path.join(__dirname, 'build'))
  app.use(express.static(path.join(__dirname, 'build')))
  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'build', 'index.html'))
  })
} catch {
  console.log(
    'No build directory detected, unable to serve app, you must compile React app first ',
  )
}

app.listen(port, () =>
  console.log(`Memes provider api is listening on http://localhost:${port}`),
)
